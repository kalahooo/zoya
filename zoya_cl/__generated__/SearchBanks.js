/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SearchBanks
// ====================================================

export type SearchBanks_banksCatalog_findBanks_correspondentAccounts = {|
  __typename: "CorrespondentAccount",
  id: any,
  number: ?string,
  status: ?AccountStatus,
|};

export type SearchBanks_banksCatalog_findBanks = {|
  __typename: "Bank",
  id: any,
  correspondentAccounts: ?Array<?SearchBanks_banksCatalog_findBanks_correspondentAccounts>,
  createdAt: ?any,
  updatedAt: ?any,
  bIK: ?string,
  parentBankId: ?any,
  name: ?string,
  fullName: ?string,
  status: ?string,
  isPartner: ?boolean,
|};

export type SearchBanks_banksCatalog = {|
  __typename: "BanksCatalogQuery",
  findBanks: ?Array<?SearchBanks_banksCatalog_findBanks>,
|};

export type SearchBanks = {|
  banksCatalog: ?SearchBanks_banksCatalog
|};

export type SearchBanksVariables = {|
  bikOrName?: ?string
|};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export type AccountStatus = "ACTIVE" | "CLOSE";

/**
 * 
 */
export type ContactResult = "FAILURE" | "INITIAL" | "SUCCESS";

/**
 * 
 */
export type ContactType = "EMAIL" | "MEETING" | "PHONECALL";

export type LeadInput = {|
  firstName?: ?string,
  lastName?: ?string,
  patronymicName?: ?string,
  phone?: ?string,
  email?: ?string,
  companyName?: ?string,
  taxId?: ?string,
  isFromExternalSource?: ?boolean,
  requestedLoanAmount?: ?any,
  requestedLoanTerm?: ?number,
  region?: ?string,
  city?: ?string,
  source?: ?string,
|};

export type ScheduledContactInput = {|
  leadId: any,
  scheduledDate?: ?any,
  factDate?: ?any,
  subject?: ?string,
  comment?: ?string,
  contactType?: ?ContactType,
  contactResult?: ?ContactResult,
  modifiedDate?: ?any,
  modifiedBy?: ?any,
  createdDate?: ?any,
  createdBy?: ?any,
|};

//==============================================================
// END Enums and Input Objects
//==============================================================