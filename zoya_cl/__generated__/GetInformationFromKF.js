/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetInformationFromKF
// ====================================================

export type GetInformationFromKF_konturFocus_get_legalAddress_city = {|
  __typename: "CityResponse",
  name: ?string,
  topo: ?string,
|};

export type GetInformationFromKF_konturFocus_get_legalAddress = {|
  __typename: "AddressResponse",
  city: ?GetInformationFromKF_konturFocus_get_legalAddress_city,
|};

export type GetInformationFromKF_konturFocus_get_lights = {|
  __typename: "Lights",
  green: ?boolean,
  yellow: ?boolean,
  red: ?boolean,
|};

export type GetInformationFromKF_konturFocus_get = {|
  __typename: "LegalEntityResponse",
  inn: ?string,
  ogrn: ?string,
  name: ?string,
  shortName: ?string,
  legalAddress: ?GetInformationFromKF_konturFocus_get_legalAddress,
  registrationDate: ?any,
  dissolutionDate: ?any,
  phones: ?Array<?string>,
  lights: ?GetInformationFromKF_konturFocus_get_lights,
  documentUrl: ?string,
|};

export type GetInformationFromKF_konturFocus = {|
  __typename: "KonturFocusQuery",
  get: ?GetInformationFromKF_konturFocus_get,
|};

export type GetInformationFromKF = {|
  konturFocus: ?GetInformationFromKF_konturFocus
|};

export type GetInformationFromKFVariables = {|
  inn?: ?string,
  ogrn?: ?string,
|};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export type AccountStatus = "ACTIVE" | "CLOSE";

/**
 * 
 */
export type ContactResult = "FAILURE" | "INITIAL" | "SUCCESS";

/**
 * 
 */
export type ContactType = "EMAIL" | "MEETING" | "PHONECALL";

export type LeadInput = {|
  firstName?: ?string,
  lastName?: ?string,
  patronymicName?: ?string,
  phone?: ?string,
  email?: ?string,
  companyName?: ?string,
  taxId?: ?string,
  isFromExternalSource?: ?boolean,
  requestedLoanAmount?: ?any,
  requestedLoanTerm?: ?number,
  region?: ?string,
  city?: ?string,
  source?: ?string,
|};

export type ScheduledContactInput = {|
  leadId: any,
  scheduledDate?: ?any,
  factDate?: ?any,
  subject?: ?string,
  comment?: ?string,
  contactType?: ?ContactType,
  contactResult?: ?ContactResult,
  modifiedDate?: ?any,
  modifiedBy?: ?any,
  createdDate?: ?any,
  createdBy?: ?any,
|};

//==============================================================
// END Enums and Input Objects
//==============================================================