/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: leadUpdate
// ====================================================

export type leadUpdate_lead = {|
  __typename: "LeadMutation",
  update: boolean,
|};

export type leadUpdate = {|
  lead: ?leadUpdate_lead
|};

export type leadUpdateVariables = {|
  id: any,
  lead?: ?LeadInput,
|};/* @flow */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * 
 */
export type AccountStatus = "ACTIVE" | "CLOSE";

/**
 * 
 */
export type ContactResult = "FAILURE" | "INITIAL" | "SUCCESS";

/**
 * 
 */
export type ContactType = "EMAIL" | "MEETING" | "PHONECALL";

export type LeadInput = {|
  firstName?: ?string,
  lastName?: ?string,
  patronymicName?: ?string,
  phone?: ?string,
  email?: ?string,
  companyName?: ?string,
  taxId?: ?string,
  isFromExternalSource?: ?boolean,
  requestedLoanAmount?: ?any,
  requestedLoanTerm?: ?number,
  region?: ?string,
  city?: ?string,
  source?: ?string,
|};

export type ScheduledContactInput = {|
  leadId: any,
  scheduledDate?: ?any,
  factDate?: ?any,
  subject?: ?string,
  comment?: ?string,
  contactType?: ?ContactType,
  contactResult?: ?ContactResult,
  modifiedDate?: ?any,
  modifiedBy?: ?any,
  createdDate?: ?any,
  createdBy?: ?any,
|};

//==============================================================
// END Enums and Input Objects
//==============================================================