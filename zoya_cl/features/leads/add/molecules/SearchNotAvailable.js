import { Header } from "../../../../ui";
import Closed from "../../../../ui/atoms/icons/Closed";
import React, { useState } from "react";
import colors from "../../../../ui/colors";
import styled from "@emotion/styled";

export default function SearchNotAvailable() {
  const [hidden, setHidden] = useState();
  return (
    !hidden && (
      <Block>
        <Header.H4>Сервис недоступен</Header.H4>
        <Message>Пожалуйста, введите информацию вручную</Message>
        <CloseButton onClick={() => setHidden(true)}>
          <Closed />
        </CloseButton>
      </Block>
    )
  );
}

const Block = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 3.8rem 3rem 3.4rem;
  position: relative;
`;
const Message = styled.div`
  font-size: 2.4rem;
  font-weight: 700;
`;
const CloseButton = styled.div`
  width: 7rem;
  height: 7rem;
  display: flex;
  cursor: pointer;
  position: absolute;
  top: 0;
  right: 0;
  align-items: center;
  justify-content: center;
`;
