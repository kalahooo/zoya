import React, { useState } from "react";
import colors from "../../../../ui/colors";
import styled from "@emotion/styled";

let timeout;

export default function SearchWithAutocomplete({
  input,
  placeholder = "",
  handleClick = fn => fn,
  results = [
    {
      name: 'ПАО "БАНК УРАЛСИБ" к/с 30101810100000000787',
      id: 1
    },
    {
      name: 'ПАО "БАНК УРАЛСИБ" к/с 30101810100000000787',
      id: 2
    },
    {
      name: 'ПАО "БАНК УРАЛСИБ" к/с 30101810100000000787',
      id: 3
    },
    {
      name: 'ПАО "БАНК УРАЛСИБ" к/с 30101810100000000787',
      id: 4
    }
  ],
  popularVariants = [
    {
      name: "ПАО «ТОЧКА»",
      id: 1
    },
    {
      name: "Сбербанк",
      id: 2
    },
    {
      name: "ПАО «Открытие»",
      id: 3
    }
  ]
}) {
  const handleChange = ev => {
    input.onChange(ev);
  };

  return (
    <Container>
      <Input
        {...input}
        hasValue={input.value}
        onChange={handleChange}
        placeholder={placeholder}
      />
      <DropResults isOpened={input.value}>
        {results &&
          results.length > 0 &&
          results.map(result => (
            <ResultItem
              key={result.id}
              onClick={() => {
                handleClick(result);
                input.onChange("");
              }}
            >
              {result.name}
              {` `}
              {result.correspondentAccounts[0]?.number
                ? `к/с ${result.correspondentAccounts[0]?.number}`
                : ""}
            </ResultItem>
          ))}

        {!results && (
          <NoResult>
            Ошибка! Указан неверный номер БИК, либо указанный БИК не найден в
            текущей базе.
          </NoResult>
        )}
      </DropResults>

      <Variants>
        {popularVariants.map(({ name, id }) => (
          <Variant key={id}>{name}</Variant>
        ))}
      </Variants>
    </Container>
  );
}

const Container = styled.div`
  position: relative;
`;
const Input = styled.input`
  display: block;
  background: #fff;
  border: solid 1px ${colors.veryLightBlue};
  border-radius: ${p => (p.hasValue ? "2px 2px 0 0" : "2px")};
  font-size: ${p => (p.hasValue ? "2.4rem" : "1.6rem")};
  font-weight: ${p => (p.hasValue ? 700 : 400)};
  font-family: inherit;
  width: 100%;
  height: 4.8rem;
  padding: 1rem 2rem;
  line-height: 2.6rem;
  position: relative;
  z-index: 10;
`;
const DropResults = styled.ul`
  position: absolute;
  z-index: 9;
  list-style: none;
  padding: 0;
  margin: 0;
  background: #fff;
  top: 0;
  left: 0;
  right: 0;
  box-shadow: 0 2px 30px 0 rgba(0, 0, 0, 0.1);
  border-radius: 2px;
  padding-top: 4.8rem;
  display: ${p => (p.isOpened ? "block" : "none")};
`;
const ResultItem = styled.li`
  font-size: 1.8rem;
  padding: 2.4rem 2rem;
  color: ${colors.purply};
  border-top: solid 1px ${colors.veryLightBlue};
  line-height: ${24 / 18};
  font-weight: 600;
  cursor: pointer;

  &:first-of-type {
    border-top: 0;
  }
`;
const NoResult = styled(ResultItem)`
  color: ${colors.darkGrey};
`;
const Variants = styled.div`
  padding: 1.2rem 2rem 0;
  font-size: 1.2rem;
  display: flex;
  flex-wrap: wrap;
`;
const Variant = styled.em`
  cursor: pointer;
  margin: 0 2.7rem 0 0;
  color: ${colors.lightGreyBlue};
`;
