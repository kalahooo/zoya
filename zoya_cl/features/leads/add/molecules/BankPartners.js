import { Header } from "../../../../ui";
import LogoRusStandart from "./uralsib.png";
import LogoUralsib from "./uralsib.png";
import React from "react";
import colors from "../../../../ui/colors";
import styled from "@emotion/styled";

export default function BankPartners({ onClick }) {
  return (
    <Block>
      <Header.H4>Выберите банк-партнер:</Header.H4>

      <List>
        <Item onClick={onClick}>
          <img alt="" src={LogoUralsib} />
        </Item>
        <Item onClick={onClick}>
          <img alt="" src={LogoRusStandart} />
        </Item>
      </List>
    </Block>
  );
}

const Block = styled.div`
  margin: 5rem 3rem;
`;
const List = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
const Item = styled.div`
  width: 12rem;
  height: 12rem;
  background: ${colors.deepLavender};
  border-radius: 0.8rem;
  margin: 0 3rem 3rem 0;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    max-width: 10rem;
    max-height: 10rem;
  }
`;
