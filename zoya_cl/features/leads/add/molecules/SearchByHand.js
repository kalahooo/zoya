import { ButtonPrimary, Header } from "../../../../ui";
import Closed from "../../../../ui/atoms/icons/Closed";
import React from "react";
import colors from "../../../../ui/colors";
import styled from "@emotion/styled";

export default function SearchByHand({
  searchString = "",
  handleClick = fn => fn
}) {
  return (
    <Block>
      <Header.H4>По запросу «{searchString}» информация не найдена</Header.H4>

      <Row>
        <Field>
          <Message>
            Ввести вручную? Или{" "}
            <MessageLink onClick={handleClick}>
              изменить поисковый запрос
            </MessageLink>
            ?
          </Message>
        </Field>
        <Action>
          <ButtonPrimary onClick={handleClick} type="button">
            Да, введу вручную
          </ButtonPrimary>
        </Action>
      </Row>
    </Block>
  );
}

const Block = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 3.8rem 3rem 3.4rem;
  position: relative;
`;
const Message = styled.div`
  font-size: 2.4rem;
  font-weight: 700;
`;
const MessageLink = styled.u`
  cursor: pointer;
`;
const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;
const Field = styled.div`
  flex: 1;
  margin-right: 10rem;
`;
const Action = styled.div`
  width: 29rem;

  button {
    width: 29rem;
    padding: 0;
  }
`;
