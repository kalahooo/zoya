import { Field, Form } from "react-final-form";
import { css } from "@emotion/core";
import Delete from "../../../../ui/atoms/icons/Delete";
import Done from "../../../../ui/atoms/icons/Done";
import HandShake from "../../../../ui/atoms/icons/HandShake";
import InputControl from "../../../../ui/atoms/misc/InputControl";
import Safebox from "../../../../ui/atoms/icons/Safebox";
import colors from "../../../../ui/colors";
import styled from "@emotion/styled";

export default function BankCard({
  handleRemove = fn => fn,
  isBankPartner = false,
  name
}) {
  return (
    <Card>
      <Head>
        <IconRemove onClick={handleRemove}>
          <IconDone>
            <Done />
          </IconDone>
          <IconDelete>
            <Delete />
          </IconDelete>
        </IconRemove>
        <Field
          name={`${name}.name`}
          render={({ input }) => <Title>{input.value}</Title>}
        />
        <Icons>
          <IconSelected>
            <Safebox />
          </IconSelected>
          <Field
            name={`${name}.isPartner`}
            render={({ input }) => (
              <IconDefault stroke={input.value}>
                <HandShake />
              </IconDefault>
            )}
          />
        </Icons>
      </Head>
      <Field
        name={`${name}.correspondentAccounts[0].number`}
        render={({ input }) => <KAccount>к/с {input.value}</KAccount>}
      />
      <div
        css={css`
          margin-top: 2rem;
        `}
      />
      {isBankPartner ? (
        <>
          <FormRow>
            <FormColumn>
              <Field
                component={InputControl}
                name="accountxx"
                placeholder="Номер расчётного счёта"
              />
            </FormColumn>
          </FormRow>
          <FormRow>
            <FormColumn>
              <Field
                component={InputControl}
                name="account_numxx"
                placeholder="Номер Договора Эквайринга"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="account_datexx"
                placeholder="Дата подписания"
              />
            </FormColumn>
          </FormRow>
        </>
      ) : (
        <Field
          component={InputControl}
          name="dfgexx"
          placeholder="Номер расчётного счёта"
        />
      )}
    </Card>
  );
}

const Card = styled.div`
  width: 57rem;
  border: solid 1px ${colors.veryLightBlue};
  margin: 0 0 2rem;
  border-radius: 1rem;
  padding: 2.3rem 8rem 2.8rem 7rem;
`;
const Title = styled.h3`
  font-weight: 700;
  font-size: 1.8rem;
  margin: 0;
`;
const Head = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  margin: 0 0 1.1rem;
`;
const IconDone = styled.div``;
const IconDelete = styled.div`
  display: none;
`;
const IconRemove = styled.div`
  cursor: pointer;
  width: 3rem;
  height: 3rem;
  position: absolute;
  top: 50%;
  left: -4.5rem;
  margin-top: -1.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 0;

  &:hover {
    ${IconDelete} {
      display: inline;
    }

    ${IconDone} {
      display: none;
    }
  }
`;
const Icons = styled.div`
  margin: 0 -6rem 0 auto;
  display: flex;
  align-items: center;
`;
const KAccount = styled.div`
  font-size: 1.4rem;
  color: ${colors.lightGreyBlue};
  margin: 0 0 1.7rem;
`;
const IconDefault = styled.div`
  margin-left: 2rem;
  cursor: pointer;

  svg [stroke] {
    stroke: ${p => (p.stroke ? colors.deepLavender : colors.lightGreyBlue)};
  }
`;
const IconSelected = styled(IconDefault)`
  color: ${colors.deepLavender};

  [fill]:not([fill="none"]) {
    fill: currentColor;
  }
`;
const AccountNumber = styled.div`
  border-bottom: solid 1px ${colors.lightBlueGrey};
  font-size: 1.6rem;
  line-height: 3.2rem;
  margin-top: 2rem;
`;
const FormRow = styled.div`
  margin: 2rem -1.5rem 0;
  display: flex;
`;

const FormColumn = styled.div`
  flex: 1;
  padding: 0 1.5rem;
`;
