import AccountTemplate from "../../../../ui/templates/Account";
import React from "react";

import LeadCreateCard from "../organisms/LeadCreateCard";
import LeadEditCard from "../organisms/LeadEditCard";

export default function LeadsAdd({ id }) {
  return (
    <AccountTemplate>
      {id ? <LeadEditCard id={id} /> : <LeadCreateCard />}
    </AccountTemplate>
  );
}

LeadsAdd.getInitialProps = function(ctx) {
  return { id: ctx.query.id };
};
