import { css } from "@emotion/core";
import styled from "@emotion/styled";
import colors from "../../../../ui/colors";

const Block = styled.div`
  width: calc(50% - 15px);
  border: ${({ withBorder }) => (withBorder ? `1px solid ${colors.veryLightBlue}` : 0)};
  background-color: ${({ withBg }) => (withBg ? colors.iceBlue : "none")};
  margin-bottom: 4rem;

  ${({ padding }) => {
    if (padding) {
      return css`
        padding: ${padding};
      `;
    }
  }}
`;

export default Block;
