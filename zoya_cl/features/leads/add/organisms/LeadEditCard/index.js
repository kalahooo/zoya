import {
  ButtonTransparent as B,
  ButtonPrimary
} from "../../../../../ui/atoms/buttons";
import { Form } from "react-final-form";
import { Header } from "../../../../../ui/atoms/typography";
import { compose, graphql } from "react-apollo";
import { css } from "@emotion/core";
import { leadGet } from "../../graphql/query/lead.graphql";
import { leadUpdate } from "../../graphql/mutations/lead.graphql";
import Aside from "../../../../../ui/atoms/blocks/Aside";
import Crumbs from "../../../../../ui/atoms/misc/Crumbs";
import Head from "../../../../../ui/atoms/blocks/Head";
import Link from "next/link";
import React from "react";
import Router from "next/router";
import Search from "../Search";
import Sections from "../sections";
import styled from "@emotion/styled";

const LeadEditCard = ({ leadUpdate, leadGet, id }) => {
  function handleSubmitForm(lead) {
    return leadUpdate({ variables: { lead, id } }).then(() => {
      Router.push("/account/leads");
    });
  }

  if (!leadGet) {
    return <div>Error</div>;
  }

  if (leadGet.loading) {
    return (
      <div
        css={css`
          padding-left: 3rem;
          padding-top: 6rem;
        `}
      >
        Загрузка...
      </div>
    );
  }

  const lead = leadGet.lead?.get || {};
  const initialValues = { ...lead, points: lead.points || [{}] };

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmitForm}
      render={({
        hasValidationErrors,
        submitting,
        handleSubmit,
        values,
        form
      }) => (
        <Layout>
          <Main>
            <Crumbs>
              <Crumbs.CrumbsLink href="/account">
                Рабочий стол
              </Crumbs.CrumbsLink>
              <Crumbs.CrumbsLink href="/account/leads">Лиды</Crumbs.CrumbsLink>
              <Crumbs.CrumbsCurrent>{lead.companyName}</Crumbs.CrumbsCurrent>
            </Crumbs>
            <Head>
              <Header.H2>Редактирование лида</Header.H2>
              <HeadAside>...</HeadAside>
            </Head>

            <Sections form={form} values={values} />
          </Main>
          <Aside>
            <Aside.Group>
              <ButtonPrimary
                disabled={hasValidationErrors || submitting}
                onClick={handleSubmit}
              >
                Сохранить
              </ButtonPrimary>
              <Link href="/account/leads" passHref>
                <ButtonTransparent>Отказ</ButtonTransparent>
              </Link>
            </Aside.Group>
          </Aside>
        </Layout>
      )}
    />
  );
};

export default compose(
  graphql(leadUpdate, { name: "leadUpdate" }),
  graphql(leadGet, { name: "leadGet" })
)(LeadEditCard);

const Layout = styled.div`
  display: flex;
  min-height: calc(100vh - 7rem);
  background: #fff;
  width: 100%;
`;
const Main = styled.div`
  flex: 1;
  padding-right: 3rem;
`;
const ButtonTransparent = styled(B.withComponent("a"))`
  margin-top: 2rem;
  text-decoration: none;
`;
const HeadAside = styled.div`
  margin-left: auto;
`;
