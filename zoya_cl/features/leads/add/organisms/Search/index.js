import { ButtonPrimary, Header } from "../../../../../ui";
import { FORM_ERROR } from "final-form";
import { Field, Form } from "react-final-form";
import { GetInformationFromKF } from "../../graphql/query/konturFocus.graphql";
import { SearchByHand, SearchNotAvailable } from "../../molecules";
import { isInnOgrn, required } from "../../../../../utils/validation-rules";
import { withApollo } from "react-apollo";
import ClearInputControl from "../../../../../ui/atoms/misc/ClearInputControl";
import React from "react";
import colors from "../../../../../ui/colors";
import styled from "@emotion/styled";

const Search = ({ handleSearch, onComplete, client }) => {
  function handleSubmitForm(values) {
    // return new Promise(resolve => {
    //   setTimeout(() => {
    //     if (values.company_search.length > 1) {
    //       resolve({ [FORM_ERROR]: "notfound" });
    //     } else if (values.company_search === "1") {
    //       resolve({ [FORM_ERROR]: "error" });
    //     } else {
    //       resolve();
    //     }
    //   }, 1500);
    // });

    //6663003127
    const key =
      values.company_search.length === 10 || values.company_search.length === 12
        ? "inn"
        : "ogrn";

    return client
      .query({
        query: GetInformationFromKF,
        variables: {
          [key]: values.company_search
        }
      })
      .then(data => {
        if (data.data.konturFocus.get) {
          return onComplete(data.data.konturFocus.get);
        }

        return { [FORM_ERROR]: "notfound" };
      });
  }
  const initialValues = {
    company_search: "6663003127"
  };
  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmitForm}
      render={({
        hasValidationErrors,
        submitting,
        handleSubmit,
        submitError,
        form,
        values
      }) =>
        !submitError ? (
          <SearchWrap>
            <Header.H4>Поисковый запрос</Header.H4>

            <FormRow>
              <FormField>
                <Field
                  component={ClearInputControl}
                  name="company_search"
                  placeholder="Укажите ИНН1 или ОГРН (ОГРНИП) или наименование ЮЛ, ФИО ИП"
                  validate={required()}
                />
              </FormField>
              <FormAction>
                <ButtonPrimary
                  disabled={hasValidationErrors || submitting}
                  onClick={handleSubmit}
                  type="button"
                >
                  Загрузить информацию
                </ButtonPrimary>
              </FormAction>
            </FormRow>
          </SearchWrap>
        ) : (
          <>
            {submitError === "error" && <SearchNotAvailable />}
            {submitError === "notfound" && (
              <SearchByHand
                handleClick={() => {
                  form.reset();
                }}
                searchString={values.company_search}
              />
            )}
          </>
        )
      }
      validate={values => {
        const errors = {};

        if (!values.company_search || (!isNaN(values.company_search) && !isInnOgrn(values.company_search))) {
          errors.notValid = "ошибка";
        }

        return errors;
      }}
    />
  );
};

const SearchWrap = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 3.8rem 3rem 3.4rem;

  h4 {
    margin-bottom: 0.6rem;
  }
`;
const FormRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;
const FormField = styled.div`
  flex: 1;
  margin-right: 10rem;
`;
const FormAction = styled.div`
  width: 29rem;

  button {
    width: 29rem;
    padding: 0;
  }
`;

export default withApollo(Search);
