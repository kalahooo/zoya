import { Field } from "react-final-form";
import { Header } from "../../../../../../ui/atoms/typography";
import { css } from "@emotion/core";
import { required } from "../../../../../../utils/validation-rules";
import Cleave from "cleave.js/dist/cleave-react-node";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionConstituent = () => {
  return (
    <>
      <InformationWrap>
        <Header.H4>Информация о компании</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="companyName"
                placeholder="Наименование"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="ogrn"
                options={{
                  blocks: [15],
                  numericOnly: true
                }}
                placeholder="ОГРН"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="inn"
                options={{
                  blocks: [12],
                  numericOnly: true
                }}
                placeholder="ИНН"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="region"
                placeholder="Регион"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>Свидетельство</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="nalogorgan"
                placeholder="Налоговый орган"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="nalogorganDate"
                placeholder="Поставлена на учет"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field component={InputControl} name="ogrn" placeholder="ОГРН" />
            </FormColumn>
            <FormColumn>
              <Field component={InputControl} name="inn" placeholder="ИНН" />
            </FormColumn>
            <FormColumn>
              <Field component={InputControl} name="kpp" placeholder="КПП" />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>Адрес (место нахождения)</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="zipcode"
                placeholder="Почтовый индек"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="city"
                placeholder="Субъект РФ"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="street"
                placeholder="Улица"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field component={InputControl} name="house" placeholder="Дом" />
            </FormColumn>
            <FormColumn>
              <Field component={InputControl} name="flat" placeholder="Офис" />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>Сведения о регистрации</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="c1"
                placeholder="Способ образования"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field component={InputControl} name="c2" placeholder="ОГРН" />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="c3"
                placeholder="Дата регистрации"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>
          Сведения о регистрирующем органе по месту нахождения юридического лица
        </Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="c1"
                placeholder="Наименование регистрирующего органа"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="c1"
                placeholder="Адрес регистрирующего органа"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>Сведения об учете в налоговом органе</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="nalogorgan"
                placeholder="Налоговый орган"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="nalogorganDate"
                placeholder="Поставлена на учет"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="inn"
                options={{
                  blocks: [12],
                  numericOnly: true
                }}
                placeholder="ИНН"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="kpp"
                options={{
                  blocks: [12],
                  numericOnly: true
                }}
                placeholder="КПП"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>
          Сведения о регистрации в качестве страхователя в территориальном
          органе Пенсионного фонда Российской Федерации
        </Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="pf"
                placeholder="Наименование территориального органа Пенсионного фонда"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="pdfate"
                placeholder="Поставлена на учет"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="pfreg"
                placeholder="Регистрационный номер"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>
          Сведения о регистрации в качестве страхователя в исполнительном органе
          Фонда социального страхования Российской Федерации
        </Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn
              css={css`
                flex: 1;
              `}
            >
              <Field
                component={InputControl}
                name="ip"
                placeholder="Наименование исполнительного органа Фонда социального страхования"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="ipfate"
                placeholder="Поставлена на учет"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="ipreg"
                placeholder="Регистрационный номер"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>

      <InformationWrap>
        <Header.H4>
          Сведения об уставном капитале (складочном капитале, уставном фонде,
          паевых взносах)
        </Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn>
              <Field component={InputControl} name="doh" placeholder="Вид" />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="dohfate"
                placeholder="Размер (в рублях)"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </InformationWrap>
    </>
  );
};

export default SectionConstituent;

const InformationWrap = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 3.8rem 3rem 3.4rem;
`;
const FormRows = styled.div`
  width: 87rem;
`;

const FormRowFlex = styled.div`
  margin: 3rem -1.5rem 0;
  display: flex;

  &:last-of-type {
    margin-bottom: 0;
  }
`;
const FormColumn = styled.div`
  padding: 0 1.5rem;
  width: 33.3333%;
`;
