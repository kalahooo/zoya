import { Field } from "react-final-form";
import { Header } from "../../../../../../ui/atoms/typography";
import { required, validEmail } from "../../../../../../utils/validation-rules";
import Cleave from "cleave.js/react";
require("cleave.js/dist/addons/cleave-phone.ru");
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionMain = () => {
  return (
    <>
      <FormGroup>
        <Header.H4>Контактная информация</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="lastName"
                placeholder="Фамилия"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="firstName"
                placeholder="Имя"
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="patronymicName"
                placeholder="Отчество"
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="phone"
                options={{
                  phone: true,
                  phoneRegionCode: "RU"
                }}
                placeholder="Телефон"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="email"
                placeholder="email"
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </FormGroup>

      <Total>
        <Field
          component={InputControl}
          inputComponent={Cleave}
          name="requestedLoanAmount"
          options={{
            numeral: true,
            delimiter: " "
          }}
          placeholder="Запрошенная сумма"
          size="lg"
          validate={required()}
        />

        <Field
          component={InputControl}
          inputComponent={Cleave}
          name="requestedLoanTerm"
          options={{
            blocks: [2],
            numericOnly: true
          }}
          placeholder="Запрошенный срок"
          size="lg"
          validate={required()}
        />
      </Total>
    </>
  );
};

// ;
export default SectionMain;

const FormGroup = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 2.8rem 3rem 3.2rem;
`;
const FormRows = styled.div`
  width: 87rem;
`;
const FormRowFlex = styled.div`
  margin: 3rem -1.5rem 0;
  display: flex;

  &:last-of-type {
    margin-bottom: 0;
  }
`;
const FormColumn = styled.div`
  padding: 0 1.5rem;
  width: 33.3333%;
`;
const Total = styled.div`
  margin-top: 8rem;
  padding-left: 3rem;
  display: flex;
  & > * {
    width: 18rem;
    margin-right: 18rem;
  }
`;
