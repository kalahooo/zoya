import { Field } from "react-final-form";
import { Header } from "../../../../../../ui/atoms/typography";
import { css } from "@emotion/core";
import { required } from "../../../../../../utils/validation-rules";
import Checkbox from "../../../../../../ui/atoms/misc/Checkbox";
import Cleave from "cleave.js/dist/cleave-react-node";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React from "react";
import Switch from "../../../../../../ui/atoms/misc/Switch";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionContract = ({ values, form }) => {
  return (
    <Groups>
      <div>
        <GrayTitle>Банки</GrayTitle>
        <KeyValue>
          <Key>Обслуживающий банк</Key>
          <Value>ПАО «Сбербанк»</Value>
        </KeyValue>
        <KeyValue>
          <Key>Банк партнер</Key>
          <Value>ПАО «БАНК УРАЛСИБ»</Value>
        </KeyValue>
      </div>

      <GrayBlock>
        <FormGroup>
          <Header.H4>Лицо, подписывающее договор</Header.H4>
          <FormRows>
            <FloatCheckbox>
              <Field component={Checkbox} name="contract_f0">
                Генеральный директор
              </Field>
            </FloatCheckbox>
            <FormRowFlex>
              <FormColumn>
                <Field
                  component={InputControl}
                  name="contract_lastName"
                  placeholder="Фамилия"
                  validate={required()}
                />
              </FormColumn>
              <FormColumn>
                <Field
                  component={InputControl}
                  name="contract_firstName"
                  placeholder="Имя"
                  validate={required()}
                />
              </FormColumn>
              <FormColumn>
                <Field
                  component={InputControl}
                  name="contract_patronymicName"
                  placeholder="Отчество"
                  validate={required()}
                />
              </FormColumn>
            </FormRowFlex>
            <FormRowFlex>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled={values.contract_f0}
                  name="contract_f1"
                  placeholder="Должность"
                  validate={required()}
                />
              </FormColumn>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled={values.contract_f0}
                  name="contract_f2"
                  placeholder="Основание полномочий"
                  validate={required()}
                />
              </FormColumn>
            </FormRowFlex>
          </FormRows>
        </FormGroup>
      </GrayBlock>

      <FormGroup>
        <Header.H4>Удостоверяющий документ</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="passport_f1"
                placeholder="Вид"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="passport_f2"
                options={{
                  blocks: [4, 6],
                  numericOnly: true
                }}
                placeholder="Серия и номер"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="passport_f3"
                placeholder="Гражданство"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn columns={2}>
              <Field
                component={InputControl}
                name="passport_f4"
                placeholder="Кем выдан"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="passport_f5"
                options={{
                  date: true,
                  delimiter: "."
                }}
                placeholder="Дата выдачи"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn columns={2}>
              <Field
                component={InputControl}
                name="passport_f6"
                placeholder="Место рождения"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="passport_f7"
                options={{
                  date: true,
                  delimiter: "."
                }}
                placeholder="Дата рождения"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={Switch}
                items={[
                  { label: "Мужской", value: 0 },
                  { label: "Женский", value: 1 }
                ]}
                name="passport_f8"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </FormGroup>

      <FormGroup>
        <Header.H4>Адрес прописки</Header.H4>
        <FormRows>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="propiska_f1"
                options={{
                  blocks: [6],
                  numericOnly: true
                }}
                placeholder="Индекс"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f2"
                placeholder="Страна"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f3"
                placeholder="Регион"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f4"
                placeholder="Район"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f5"
                placeholder="Населенный. пункт"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f6"
                placeholder="Улица"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f7"
                placeholder="Дом"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f8"
                placeholder="Корпус"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f9"
                placeholder="Строение"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                name="propiska_f10"
                placeholder="Квартира/Офис"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </FormGroup>

      <FormGroup>
        <Header.H4>Адрес проживания</Header.H4>
        <FormRows>
          <FloatCheckbox>
            <Field component={Checkbox} name="projivanie_f0">
              Совпадает с адресом прописки
            </Field>
          </FloatCheckbox>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                inputComponent={Cleave}
                name={values.projivanie_f0 ? "propiska_f1" : "projivanie_f1"}
                options={{
                  blocks: [6],
                  numericOnly: true
                }}
                placeholder="Индекс"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f2" : "projivanie_f2"}
                placeholder="Страна"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f3" : "projivanie_f3"}
                placeholder="Регион"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f4" : "projivanie_f4"}
                placeholder="Район"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f5" : "projivanie_f5"}
                placeholder="Населенный. пункт"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f6" : "projivanie_f6"}
                placeholder="Улица"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f7" : "projivanie_f7"}
                placeholder="Дом"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
          <FormRowFlex>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f8" : "projivanie_f8"}
                placeholder="Корпус"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f9" : "projivanie_f9"}
                placeholder="Строение"
                validate={required()}
              />
            </FormColumn>
            <FormColumn>
              <Field
                component={InputControl}
                disabled={values.projivanie_f0}
                name={values.projivanie_f0 ? "propiska_f10" : "projivanie_f10"}
                placeholder="Квартира/Офис"
                validate={required()}
              />
            </FormColumn>
          </FormRowFlex>
        </FormRows>
      </FormGroup>

      <div>
        <Header.H4
          css={css`
            padding-left: 3rem;
          `}
        >
          Адреса точек продаж
        </Header.H4>
        {values.points.map((p, index) => (
          <GrayBlock key={index}>
            <FormGroup>
              <FormRows>
                <FormRowFlex
                  css={css`
                    margin-top: 0;
                  `}
                >
                  <FormColumn columns={2}>
                    <Field
                      component={InputControl}
                      name={`tochki_${index}_f1`}
                      placeholder="Адрес"
                      validate={required()}
                    />
                  </FormColumn>
                </FormRowFlex>
                <FormRowFlex>
                  <FormColumn>
                    <Field
                      component={InputControl}
                      name={`tochki_${index}_f2`}
                      placeholder="Номер Договора Эквайринга"
                      validate={required()}
                    />
                  </FormColumn>
                  <FormColumn>
                    <Field
                      component={InputControl}
                      inputComponent={Cleave}
                      name={`tochki_${index}_f3`}
                      options={{
                        date: true,
                        delimiter: "."
                      }}
                      placeholder="Дата подписания"
                      validate={required()}
                    />
                  </FormColumn>
                </FormRowFlex>
              </FormRows>
            </FormGroup>
          </GrayBlock>
        ))}
        <AddPoint
          onClick={() => form.change("points", values.points.concat({}))}
        >
          + Добавить еще один адрес точки
        </AddPoint>
      </div>
    </Groups>
  );
};

export default SectionContract;

const Groups = styled.div`
  & > * {
    margin-top: 5rem;
    &:first-of-type {
      margin-top: 0;
    }
  }
`;
const FormGroup = styled.div`
  margin: 0 0 4rem;
  padding-left: 3rem;
  padding-top: 2.8rem;
`;
const FormRows = styled.div`
  width: 87rem;
  position: relative;
`;
const FormRowFlex = styled.div`
  margin: 3rem -1.5rem 0;
  display: flex;

  &:last-of-type {
    margin-bottom: 0;
  }
`;
const FormColumn = styled.div`
  padding: 0 1.5rem;
  width: ${({ columns = 1 }) => `${33.3333 * columns}%`};
`;
const FloatCheckbox = styled.div`
  position: absolute;
  right: 0rem;
  top: -5rem;
  text-align: right;
`;
const GrayBlock = styled.div`
  background: ${colors.iceBlue};
  ${FormGroup} {
    padding-bottom: 3.2rem;
  }
`;
const GrayTitle = styled(GrayBlock)`
  font-weight: bold;
  padding-left: 3rem;
  line-height: 4rem;
  margin-bottom: 1rem;
  width: 90rem;
`;
const KeyValue = styled.div`
  display: flex;
  line-height: 4rem;
  padding-left: 3rem;
`;
const Key = styled.div`
  color: ${colors.lightGreyBlue};
  width: 25rem;
`;
const Value = styled.div``;
const AddPoint = styled.span`
  color: ${colors.deepLavender};
  font-weight: bold;
  margin-left: 3rem;
  cursor: pointer;
`;
