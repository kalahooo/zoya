import AddFile from "../../../../../../ui/atoms/icons/AddFile";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionDocuments = () => {
  return (
    <div>
      <GrayTitle>Юридические документы</GrayTitle>
      <DocumentsList>
        <Doc>
          <FileIcon>
            <AddFile />
          </FileIcon>
          <DocTitle>
            <Title>Новый документ</Title>
            <Description>Загрузить</Description>
          </DocTitle>
        </Doc>
        <Doc>
          <FileIcon>
            <AddFile />
          </FileIcon>
          <DocTitle>
            <Title>Новый документ</Title>
            <Description>Загрузить</Description>
          </DocTitle>
        </Doc>
      </DocumentsList>
      <GrayTitle>ЕГРН</GrayTitle>
      <DocumentsList>
        <Doc>
          <FileIcon>
            <AddFile />
          </FileIcon>
          <DocTitle>
            <Title>Новый документ</Title>
            <Description>Загрузить</Description>
          </DocTitle>
        </Doc>
      </DocumentsList>
      <GrayTitle>От Контур.Фокус</GrayTitle>
      <DocumentsList>
        <Doc>
          <FileIcon>
            <AddFile />
          </FileIcon>
          <DocTitle>
            <Title>Новый документ</Title>
            <Description>Загрузить</Description>
          </DocTitle>
        </Doc>
      </DocumentsList>
    </div>
  );
};

export default SectionDocuments;

const GrayBlock = styled.div`
  background: ${colors.iceBlue};
`;
const GrayTitle = styled(GrayBlock)`
  font-weight: bold;
  padding-left: 3rem;
  line-height: 4rem;
  width: 90rem;
`;
const DocumentsList = styled.div`
  padding: 2rem 0 2rem 3rem;
  display: flex;
  & > * {
    margin-right: 3rem;
  }
`;
const Doc = styled.div`
  width: 12rem;
  height: 12rem;
  border-radius: 0.8rem;
  border: solid 1px ${colors.lightBlueGrey};
  position: relative;
  cursor: pointer;
`;
const FileIcon = styled.div`
  position: absolute;
  width: 3.6rem;
  height: 3.6rem;
  border: solid 1px ${colors.lightBlueGrey};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0.8rem;
  left: 1.2rem;
  top: 1.2rem;
`;
const DocTitle = styled.div`
  left: 1.2rem;
  position: absolute;
  bottom: 1rem;
  font-size: 0.75em;
`;
const Title = styled.div`
  font-weight: bold;
`;
const Description = styled.div``;
