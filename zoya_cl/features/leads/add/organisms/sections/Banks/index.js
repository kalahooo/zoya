import { BankCard, BankPartners } from "../../../molecules";
import { Field, Form } from "react-final-form";
import { FieldArray } from "react-final-form-arrays";
import { Header } from "../../../../../../ui";
import { Query, withApollo } from "react-apollo";
import { SearchBanks } from "../../../graphql/query/banksCatalog.graphql";
import React, { useState } from "react";
import SearchWithAutocomplete from "../../../molecules/SearchWithComplete";
import arrayMutators from "final-form-arrays";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionBanks = ({ client }) => {
  // <pre>{JSON.stringify(values, 0, 2)}</pre>

  return (
    <>
      <Form
        mutators={{
          ...arrayMutators
        }}
        onSubmit={values => console.log(values)}
        render={({ values, form }) => (
          <>
            <AddBankForm>
              <Header.H4>Добавить обслуживающий банк</Header.H4>

              <Query
                query={SearchBanks}
                skip={!values.bikOrName || values.bikOrName.length < 3}
                variables={{ bikOrName: values.bikOrName }}
              >
                {(data, loading, error) => {
                  return (
                    <>
                      <SearchField>
                        <Field
                          component={SearchWithAutocomplete}
                          disabled={!!values?.bank_list?.length}
                          handleClick={bankData =>
                            form.mutators.push("bank_list", bankData)
                          }
                          name="bikOrName"
                          placeholder="Введите БИК или название Банка"
                          results={data?.data?.banksCatalog?.findBanks || []}
                        />
                      </SearchField>
                    </>
                  );
                }}
              </Query>
            </AddBankForm>

            <FieldArray name="bank_list">
              {({ fields }) => (
                <>
                  {fields.map((name, index) => (
                    <BankCard
                      handleRemove={() => fields.remove(index)}
                      key={name}
                      name={name}
                    />
                  ))}
                </>
              )}
            </FieldArray>

            {/*<BankPartners onClick={fn => fn} />*/}
          </>
        )}
      />
    </>
  );
};

const AddBankForm = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 2rem 3rem 1.4rem;
  position: relative;
`;

const SearchField = styled.div`
  width: 72rem;
`;

export default withApollo(SectionBanks);
