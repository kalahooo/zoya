import { Block } from "../../../atoms";
import { Field, Form } from "react-final-form";
import { Header, theme } from "../../../../../../ui";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

export default ({ onSubmit = fn => fn }) => (
  <Block padding="3rem 3rem 1rem" withBg>
    <Header.H2>Заявка</Header.H2>

    <BlockContent>
      <Form
        initialValues={{
          a: "0",
          b: "0",
          c: "0",
          d: "0",
          e: "0",
          f: "0",
        }}
        onSubmit={onSubmit}
        render={({}) => (
          <>
            <FieldContainer>
              <Field
                component={InputControl}
                name="a"
                placeholder="Срок займа, мес"
                size="big"
              />
            </FieldContainer>
            <FieldContainer>
              <Field
                component={InputControl}
                name="b"
                placeholder="Сумма займа, руб"
                size="big"
              />
            </FieldContainer>
            <FieldContainer>
              <Field
                component={InputControl}
                name="c"
                placeholder="Удержание, %"
                size="big"
              />
            </FieldContainer>
            <FieldContainer>
              <Field
                component={InputControl}
                name="d"
                placeholder="Процентная ставка, %"
                size="big"
              />
            </FieldContainer>
            <FieldContainer>
              <Field
                component={InputControl}
                name="e"
                placeholder="Сумма %, руб"
                size="big"
              />
            </FieldContainer>
            <FieldContainer>
              <Field
                component={InputControl}
                name="f"
                placeholder="Сумма % в день, руб"
                size="big"
              />
            </FieldContainer>
          </>
        )}
      />
    </BlockContent>
  </Block>
);

const BlockContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding-top: 4rem;
`;

const FieldContainer = styled.div`
  width: 33.3333%;
  padding-right: 5rem;
  margin-bottom: 5rem;
`;
