import { Block } from "../../../atoms";
import { Header, theme } from "../../../../../../ui";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";
import Table from "../../../../../../ui/atoms/misc/Table";

export default () => (
  <Block padding="3rem 3rem 1rem" withBg>
    <Header.H2>Досрочное погашение</Header.H2>

    <Table>
      <TableRow>
        <TableHeadColumn>Срок досрочного погашения</TableHeadColumn>
        <TableHeadColumn>Сниженная процентная ставка</TableHeadColumn>
        <TableHeadColumn>Сниженная сумма процентов</TableHeadColumn>
        <TableHeadColumn>Возврат процентов досрочно</TableHeadColumn>
      </TableRow>

      <TableRow>
        <Table.Column>5 мес.</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>32 200</Table.Column>
      </TableRow>

      <TableRow>
        <Table.Column>3 мес.</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>32 200</Table.Column>
      </TableRow>

      <TableRow>
        <Table.Column>2 мес.</Table.Column>
        <Table.Column>21.5%</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>94 200</Table.Column>
      </TableRow>
    </Table>
  </Block>
);

const TableRow = styled(Table.Row)`
  height: 6rem;
  padding: 1rem 0 .9rem;

  &:not(:last-of-type) {
    border-bottom: 1px solid ${colors.lightBlueGrey};
  }
`;

const TableHeadColumn = styled(Table.HeadColumn)`
  padding-right: 2rem;
`;

