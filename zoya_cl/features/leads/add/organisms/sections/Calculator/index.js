import { Header } from "../../../../../../ui";
import Acquiring from "./Acquiring";
import ParamsTable from "./ParamsTable";
import ParamsAdd from "./ParamsAdd";
import Claim from "./Claim";
import Repayment from "./Repayment";
import React from "react";
import styled from "@emotion/styled";

const SectionCalculator = () => {
  return (
    <>
      <Acquiring />
      <Header.H2>Параметры</Header.H2>
      <Params>
        <ParamsTable />
        <ParamsAdd />
        <Claim />
        <Repayment />
      </Params>
    </>
  );
};

export default SectionCalculator;

const Params = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin-bottom: 5rem;
`;
