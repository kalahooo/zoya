import { Block } from "../../../atoms";
import { Field, Form } from "react-final-form";
import { Label, colors, theme, ButtonPrimary, ButtonTransparent } from "../../../../../../ui";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React, { useState } from "react";
import styled from "@emotion/styled";
import TextRadio from "../../../../../../ui/atoms/misc/TextRadio";
import Modal from "../../../../../../ui/atoms/misc/Modal";
import Collapser from "../../../../../../ui/atoms/misc/Collapser";
import ArrowUp from "../../../../../../ui/atoms/icons/ArrowUp";
import Checkbox from "../../../../../../ui/atoms/misc/Checkbox";

export default ({ onSubmit = fn => fn }) => {
  const [isModalOpened, toggleModal] = useState(false);

  return (
    <Block padding="1rem 7rem 3rem 4rem" withBorder>
      <Form
        onSubmit={onSubmit}
        render={({ }) => (
          <>
            <Label>Выбрать вид деятельности</Label>
            <KindOfActivity onClick={() => toggleModal(true)}>
              Посуда, Стройматериалы, Одежда
            </KindOfActivity>
            <Space />
            <Label>Тип помещения</Label>
            <Field component={InputControl} name="placeType" notShrink />
            <Space />
            <Label>В собственности</Label>
            <TextRadio name="ownership" value="room" label="Помещение" />
            <TextRadio name="ownership" value="stock" label="Склад" />
            <Space />
            <Label>Конкуренция</Label>
            <Field
              component={InputControl}
              name="competition"
              placeholder="Наличие конкурентов в округе (в радиусе 500 м)"
              notShrink
            />
            <Discount>
              <Field
                component={InputControl}
                name="discount"
                placeholder="Дисконт"
                size="big"
              />
            </Discount>

            {
              isModalOpened && (
                <Modal onCancel={() => toggleModal(false)}>
                  <ModalContent>
                    <Collapser opened>
                      {({ isOpened, toggle }) => {
                        return (
                          <>
                            <CollapseHeader onClick={() => toggle(!isOpened)}>
                              Торговля
                              <CollapseBtn isOpened={isOpened}>
                                <ArrowUp />
                              </CollapseBtn>
                            </CollapseHeader>
                            <CollapseContent isOpened={isOpened}>
                              <FieldsGroup>
                                {
                                  ["one", "two", "three", "four", "five"].map(i => (
                                    <FieldsGroupItem key={i}>
                                      <Field component={Checkbox} name="name1" value={i}>{i}</Field>
                                    </FieldsGroupItem>
                                  ))
                                }
                              </FieldsGroup>
                            </CollapseContent>
                          </>
                        )
                      }}
                    </Collapser>

                    <Collapser>
                      {({ isOpened, toggle }) => {
                        return (
                          <>
                            <CollapseHeader onClick={() => toggle(!isOpened)}>
                              Услуги
                              <CollapseBtn isOpened={isOpened}>
                                <ArrowUp />
                              </CollapseBtn>
                            </CollapseHeader>
                            <CollapseContent isOpened={isOpened}>
                              <FieldsGroup>
                                {
                                  ["one", "two", "three", "four", "five"].map(i => (
                                    <FieldsGroupItem key={i}>
                                      <Field component={Checkbox} name="name1" value={i}>{i}</Field>
                                    </FieldsGroupItem>
                                  ))
                                }
                              </FieldsGroup>
                            </CollapseContent>
                          </>
                        )
                      }}
                    </Collapser>

                    <Collapser>
                      {({ isOpened, toggle }) => {
                        return (
                          <>
                            <CollapseHeader onClick={() => toggle(!isOpened)}>
                              Производство
                              <CollapseBtn isOpened={isOpened}>
                                <ArrowUp />
                              </CollapseBtn>
                            </CollapseHeader>
                            <CollapseContent isOpened={isOpened}>
                              <FieldsGroup>
                                {
                                  ["one", "two", "three", "four", "five"].map(i => (
                                    <FieldsGroupItem key={i}>
                                      <Field component={Checkbox} name="name1" value={i}>{i}</Field>
                                    </FieldsGroupItem>
                                  ))
                                }
                              </FieldsGroup>
                            </CollapseContent>
                          </>
                        )
                      }}
                    </Collapser>

                    <ModalAction>
                      <ButtonPrimary type="button">Сохранить</ButtonPrimary>
                      <ButtonTransparent type="button" onClick={() => toggleModal(false)}>Отменить</ButtonTransparent>
                    </ModalAction>
                  </ModalContent>
                </Modal>
              )
            }
          </>
        )}
      />
    </Block>
  );
};

const Discount = styled.div`
  width: 12rem;
  margin-top: 3rem;
`;

const Space = styled.div`
  height: 2.2rem;
`;

const KindOfActivity = styled.div`
  border-bottom: solid 1px rgba(0, 0, 0, .1);
  font-size: 1.6rem;
  min-height: 3.4rem;
  display: flex;
  align-items: cetner;
  flex-wrap: wrap;
`;

const ModalContent = styled.div`
  margin: -7rem -10rem -10rem;
  padding: 1.5rem 1.5rem 4.5rem;
  width: 57rem;
`;
const CollapseBtn = styled.div`
  position: absolute;
  right: 1rem;
  top: 0;
  bottom: 0;
  margin: auto 0;
  height: 3rem;
  width: 3rem;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transform: ${p => p.isOpened ? "rotate(0)" : "rotate(180deg)"};
  transition: all .4s;
`;
const CollapseHeader = styled.div`
  background: ${colors.iceBlue};
  font-size: 1.6rem;
  padding: 0 3rem;
  line-height: 4rem;
  position: relative;
  font-weight: 700;
  cursor: pointer;
`;
const CollapseContent = styled.div`
  max-height: ${p => p.isOpened ? "100rem" : "0"};
  transition: ${p => p.isOpened ? "max-height 1s" : "max-height .3s"};
  overflow: hidden;
  margin: 0 0 1rem;
`;
const FieldsGroup = styled.div`
  padding: 3rem 3rem .1rem;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;
const FieldsGroupItem = styled.div`
  width: 46%;
  margin: 0 0 2rem;
`;
const ModalAction = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 3rem 3rem 0;
  
  button {
    width: 100%;
    max-width: calc(50% - 1rem);
  }
`;