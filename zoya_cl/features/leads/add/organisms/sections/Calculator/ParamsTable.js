import { Block } from "../../../atoms";
import { Field, Form } from "react-final-form";
import { theme } from "../../../../../../ui";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import Table from "../../../../../../ui/atoms/misc/Table";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

export default ({ onSubmit = fn => fn }) => (
  <Block padding="1rem 0 3rem" withBorder>
    <Table>
      <Table.Row>
        <Table.HeadColumn>Срок аванса</Table.HeadColumn>
        <Table.HeadColumn>Сумма займа</Table.HeadColumn>
        <Table.HeadColumn>Процентная ставка</Table.HeadColumn>
        <Table.HeadColumn>Сумма процентов</Table.HeadColumn>
      </Table.Row>

      <Table.Row>
        <Table.Column>5</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>32 200</Table.Column>
      </Table.Row>

      <Table.Row>
        <Table.Column>6</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>32 200</Table.Column>
      </Table.Row>

      <Table.Row>
        <Table.Column>7</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>32 200</Table.Column>
      </Table.Row>
    </Table>

    <Form
      onSubmit={onSubmit}
      render={({}) => (
        <Retention>
          <Field
            component={InputControl}
            name="retention"
            placeholder="Удержание"
            size="big"
          />
        </Retention>
      )}
    />
  </Block>
);

const Retention = styled.div`
  width: 12rem;
  padding-left: 3rem;
  margin-top: 4.8rem;
`;
