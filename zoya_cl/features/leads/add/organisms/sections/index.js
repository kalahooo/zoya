import { css } from "@emotion/core";
import React from "react";
import SectionBanks from "./Banks";
import SectionCalculator from "./Calculator";
import SectionConstituent from "./Constituent";
import SectionContract from "./Contract";
import SectionDocuments from "./Documents";
import SectionMain from "./Main";
import Tabs from "../../../../../ui/organisms/Tabs";
import colors from "../../../../../ui/colors";
import styled from "@emotion/styled";

const availableTabs = [
  "Основное",
  "Калькулятор",
  "Учредительные данные",
  "Банки",
  "Договор",
  "Скоринг",
  "Документы"
];
const components = [
  SectionMain,
  SectionCalculator,
  SectionConstituent,
  SectionBanks,
  SectionContract,
  null,
  SectionDocuments
];

const Sections = ({ values, form }) => {
  return (
    <Tabs defaultIndex={0}>
      {({ selectedIndex, setSelectedIndex }) => {
        const Component = components[selectedIndex];

        return (
          <>
            <PageTabs>
              {availableTabs.map((t, index) => (
                <PageTabsItem
                  active={selectedIndex === index}
                  key={t}
                  onClick={() => setSelectedIndex(index)}
                >
                  {t}
                </PageTabsItem>
              ))}
            </PageTabs>

            {Component ? <Component form={form} values={values} /> : null}
          </>
        );
      }}
    </Tabs>
  );
};

export default Sections;

const PageTabs = styled.div`
  height: 3.4rem;
  display: flex;
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  border-radius: 2em;
  font-weight: 700;
  font-size: 1.4rem;
  padding: 0 1rem;
`;
const PageTabsItem = styled.div`
  padding: 0 2rem;
  border-radius: 2rem;
  margin: 0 1rem 0 0;
  cursor: pointer;
  transition: all 0.25s;
  line-height: 3.4rem;
  color: ${colors.lightGreyBlue};

  ${({ active }) =>
    active &&
    css`
      color: #fff;
      background: ${colors.deepLavender};
    `}
`;
