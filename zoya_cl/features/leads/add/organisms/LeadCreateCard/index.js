// @flow
import {
  ButtonTransparent as B,
  ButtonPrimary
} from "../../../../../ui/atoms/buttons";
import { Form } from "react-final-form";
import { Header } from "../../../../../ui/atoms/typography";
import { compose, graphql, withApollo } from "react-apollo";
import { leadCreate } from "../../graphql/mutations/lead.graphql";
import Aside from "../../../../../ui/atoms/blocks/Aside";
import Crumbs from "../../../../../ui/atoms/misc/Crumbs";
import Head from "../../../../../ui/atoms/blocks/Head";
import Link from "next/link";
import React from "react";
import Router from "next/router";
import Search from "../Search";
import Sections from "../sections";
import styled from "@emotion/styled";

const LeadCreateCard = ({ client, leadCreate, getFromKonturFocus }) => {
  function handleSubmitForm(lead) {
    return leadCreate({ variables: { lead } }).then(v => {
      // console.log(v);
      Router.push("/account/leads");
    });
  }

  const initialValues = { points: [{}] };

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmitForm}
      render={({
        hasValidationErrors,
        submitting,
        handleSubmit,
        form,
        values
      }) => (
        <Layout>
          <Main>
            <Crumbs>
              <Crumbs.CrumbsLink href="/account">
                Рабочий стол
              </Crumbs.CrumbsLink>
              <Crumbs.CrumbsLink href="/account/leads">Лиды</Crumbs.CrumbsLink>
              <Crumbs.CrumbsCurrent>Добавление лида</Crumbs.CrumbsCurrent>
            </Crumbs>
            <Head>
              <Header.H2>Добавление лида</Header.H2>
              <HeadAside>...</HeadAside>
            </Head>

            <Search
              onComplete={(
                searchResult: GetInformationFromKF_konturFocus_get
              ) => {
                console.log(searchResult);
                return form.initialize({
                  ...values,
                  ...searchResult,
                  companyName: searchResult.name,
                  phone: searchResult.phones ? searchResult.phones[0] : "'",
                  region: searchResult.legalAddress.region.name,
                  nalogorgan: searchResult.nalogName,
                  nalogorganDate: searchResult.nalogRegistrationDate,
                  zipcode: searchResult.legalAddress.zipCode,
                  city: searchResult.legalAddress.city.name,
                  street: searchResult.legalAddress.street.name,
                  house: searchResult.legalAddress.house.name,
                  flat: searchResult.legalAddress.flat.name
                });
              }}
            />

            <Sections form={form} values={values} />
          </Main>
          <Aside>
            <Aside.Group>
              <ButtonPrimary
                disabled={hasValidationErrors || submitting}
                onClick={handleSubmit}
              >
                Сохранить
              </ButtonPrimary>
              <Link href="/account/leads" passHref>
                <ButtonTransparent>Отказ</ButtonTransparent>
              </Link>
            </Aside.Group>
          </Aside>
        </Layout>
      )}
    />
  );
};

export default compose(
  withApollo,
  graphql(leadCreate, { name: "leadCreate" })
)(LeadCreateCard);

const Layout = styled.div`
  display: flex;
  min-height: calc(100vh - 7rem);
  background: #fff;
  width: 100%;
`;
const Main = styled.div`
  flex: 1;
  padding-right: 3rem;
`;
const ButtonTransparent = styled(B.withComponent("a"))`
  margin-top: 2rem;
  text-decoration: none;
`;
const HeadAside = styled.div`
  margin-left: auto;
`;
