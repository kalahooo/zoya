// @flow
import Edit from "../../../../../ui/atoms/icons/Edit";
// import ErrorIcon from "../../../../../ui/atoms/icons/ErrorIcon";
import { format } from "date-fns";
import Link from "next/link";
import Pie from "../../../../../ui/atoms/misc/Pie";
import Plus from "../../../../../ui/atoms/icons/Plus";
import React from "react";
import Tooltip from "../../../../../ui/atoms/misc/Tooltip";
import Undone from "../../../../../ui/atoms/icons/Undone";
import colors from "../../../../../ui/colors";
import styled from "@emotion/styled";
import type { leadSearch_lead_search } from "../../../../../__generated__/leadSearch";

const LeadLine = ({
  filled = 0,
  onAddTask,
  ...l
}: {
  filled: number,
  l: leadSearch_lead_search,
  onAddTask: Function
}) => {
  function handleClickAddTask() {
    onAddTask(l.id);
  }

  const createdAt = format(
    l.created_at ? new Date(l.created_at) : new Date(),
    "DD.MM.YY"
  );

  return (
    <LeadLineWrap>
      <LeadLineWrapInner>
        <Status>
          <Undone />
        </Status>
        <Data>
          <Top>
            <Title>
              <Link href={`/account/leads/view?id=${l.id}`} passHref>
                <a>{l.companyName}</a>
              </Link>
            </Title>
            <Control>
              <Tooltip tooltipText="Редактировать">
                <Link href={`/account/leads/edit?id=${l.id}`} passHref>
                  <a>
                    <Edit />
                  </a>
                </Link>
              </Tooltip>

              <Tooltip tooltipText="Добавить задачу">
                <span onClick={handleClickAddTask}>
                  <Plus />
                </span>
              </Tooltip>
            </Control>
          </Top>
          <Information>
            <About>
              {l.lastName} {l.firstName}
            </About>
            <About>{l.requestedLoanAmount} ₽</About>
            <About>{l.requestedLoanTerm} мес</About>
            <About>создан {createdAt}</About>
            <About>Иван</About>
            <Lights>
              <Light color="green" />
              <Light color="yellow" />
              <Light color="gray" />
            </Lights>
            <Percent>
              <Tooltip tooltipText={`Контакт ${filled}%`}>
                <Pie value={filled} />
              </Tooltip>
              <PercentValue>{filled}%</PercentValue>
            </Percent>
          </Information>
        </Data>
      </LeadLineWrapInner>
    </LeadLineWrap>
  );
};

export default LeadLine;

const Control = styled.div`
  display: flex;
  align-items: center;
  margin-left: 4rem;
  opacity: 0;
  transition: opacity 0.1s ease-in;
  > * {
    margin-right: 2rem;
    cursor: pointer;
  }
`;
const LeadLineWrapInner = styled.div`
  height: 10rem;
  display: flex;
  align-items: center;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;
const LeadLineWrap = styled.div`
  padding: 0 3rem;
  background: transparent;
  transition: background 0.1s ease-in;
  margin-top: -1px;
  &:hover {
    background: ${colors.iceBlue};
    ${Control} {
      opacity: 1;
    }
    ${LeadLineWrapInner} {
      border-bottom-color: ${colors.iceBlue};
      position: relative;
    }
  }
  &:last-of-type {
    ${LeadLineWrapInner} {
      border-bottom: none;
    }
  }
`;
const Status = styled.div`
  width: 5rem;
  height: 5rem;
`;
const Data = styled.div`
  width: 100%;
`;
const Top = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 1rem;
`;
const Title = styled.div`
  font-size: 1.3em;
  a {
    text-decoration: none;
    color: inherit;
  }
`;
const Information = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;
const About = styled.div`
  color: ${colors.lightGreyBlue};
  margin-right: 3rem;
`;
const Lights = styled.div`
  margin-left: auto;
  display: flex;
`;
const Light = styled.div`
  width: 1.6rem;
  height: 1.6rem;
  border-radius: 50%;
  margin-left: 0.5rem;
  background: ${({ color }) => {
    if (color === "green") {
      return colors.aquaMarine;
    } else if (color === "yellow") {
      return colors.macaroniAndCheese;
    } else if (color === "gray") {
      return colors.veryLightBlue;
    }
    return "black";
  }};
`;
const Percent = styled.div`
  margin-left: 4rem;
`;
const PercentValue = styled.div`
  font-weight: bold;
  color: ${colors.deepLavender};
  font-size: 1.1em;
  display: inline-block;
  vertical-align: middle;
  margin-left: 1rem;
`;
