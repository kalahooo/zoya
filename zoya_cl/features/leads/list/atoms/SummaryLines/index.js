import { css } from "@emotion/core";
import React from "react";
import colors from "../../../../../ui/colors";
import styled from "@emotion/styled";

const SummaryLines = styled.div`
  margin: -1.5rem 0;
`;

SummaryLines.Item = ({ title, children, active }) => {
  return (
    <SummaryLineWrap>
      <Title active={active}>{title}</Title>
      {children}
    </SummaryLineWrap>
  );
};

export default SummaryLines;

const SummaryLineWrap = styled.div`
  height: 4rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 23rem;
`;
const Title = styled.span`
  color: ${colors.lightGreyBlue};
  font-weight: bold;

  ${({ active }) =>
    active &&
    css`
      color: ${colors.deepLavender};
    `}
`;
