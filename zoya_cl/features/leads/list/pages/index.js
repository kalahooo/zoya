import { ButtonPrimary as B } from "../../../../ui/atoms/buttons";
import { Header } from "../../../../ui/atoms/typography";
import AccountTemplate from "../../../../ui/templates/Account";
import Aside from "../../../../ui/atoms/blocks/Aside";
import Crumbs from "../../../../ui/atoms/misc/Crumbs";
import Head from "../../../../ui/atoms/blocks/Head";
import LeadList from "../organisms/LeadList";
import LeadStatistics from "../organisms/LeadStatistics";
import Link from "next/link";
import React from "react";
import styled from "@emotion/styled";

const LeadsList = ({ action }) => {
  return (
    <AccountTemplate>
      <WithSidebar>
        <Main>
          <Crumbs>
            <Crumbs.CrumbsLink href="/account">Рабочий стол</Crumbs.CrumbsLink>
            <Crumbs.CrumbsCurrent>Лиды</Crumbs.CrumbsCurrent>
          </Crumbs>

          <Head>
            <Header.H2>Все лиды</Header.H2>
          </Head>

          <LeadList action={action} />
        </Main>

        <Aside>
          <Aside.Group>
            <Link href="/account/leads/add" passHref>
              <ButtonPrimary>Добавить лида</ButtonPrimary>
            </Link>
          </Aside.Group>

          <LeadStatistics />
        </Aside>
      </WithSidebar>
    </AccountTemplate>
  );
};

export default LeadsList;

const WithSidebar = styled.div`
  display: flex;
`;
const Main = styled.div`
  flex: 1;
`;

const ButtonPrimary = B.withComponent("a");
