import Aside from "../../../../../ui/atoms/blocks/Aside";
import React from "react";
import SummaryLines from "../../atoms/SummaryLines";

const { Item } = SummaryLines;

const LeadStatistics = () => {
  return (
    <>
      <Aside.Group>
        <SummaryLines>
          <Item active title="Все">
            3
          </Item>
          <Item title="Назначенные мне">0</Item>
        </SummaryLines>
      </Aside.Group>

      <Aside.Group>
        <SummaryLines>
          <Item title="Банкротство">0</Item>
          <Item title="Ликвидация">0</Item>
          <Item title="Реорганизация">0</Item>
        </SummaryLines>
      </Aside.Group>

      <Aside.Group>
        <SummaryLines>
          <Item title="Крупные предприятия">0</Item>
          <Item title="Средние">0</Item>
          <Item title="Малые">0</Item>
          <Item title="Микро">0</Item>
        </SummaryLines>
      </Aside.Group>

      <Aside.Group>
        <SummaryLines>
          <Item title="Активные">0</Item>
          <Item title="Отказ лида">0</Item>
          <Item title="Наш отказ">0</Item>
        </SummaryLines>
      </Aside.Group>
    </>
  );
};

export default LeadStatistics;
