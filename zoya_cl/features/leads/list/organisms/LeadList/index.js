// @flow
import { css } from "@emotion/core";
import { graphql } from "react-apollo";
import { leadSearch } from "../../graphql/query/leads.graphql";
import AddTaskModal from "../../../../../ui/organisms/AddTaskModal";
import LeadLine from "../../atoms/LeadLine";
import React, { useState } from "react";
import styled from "@emotion/styled";

const LeadList = ({ leadSearch, action }) => {
  const [selectedLeadToTask, setSelectedLeadToTask] = useState(
    action === "ask"
  );

  function handleClickAddTask(value) {
    setSelectedLeadToTask(value);
  }

  if (leadSearch.loading) {
    return (
      <div
        css={css`
          padding-left: 3rem;
        `}
      >
        Загрузка...
      </div>
    );
  }
  const leads = leadSearch.lead?.search || [];

  return (
    <Leads>
      {leads.map(l => (
        <LeadLine filled="5" key={l.id} onAddTask={handleClickAddTask} {...l} />
      ))}

      {selectedLeadToTask && (
        <AddTaskModal
          leadId={selectedLeadToTask}
          onCancel={() => setSelectedLeadToTask()}
          onSuccess={() => setSelectedLeadToTask()}
        />
      )}
    </Leads>
  );
};

export default graphql(leadSearch, { name: "leadSearch" })(LeadList);

const Leads = styled.div`
  padding: 0 3rem 0 0rem;
`;
