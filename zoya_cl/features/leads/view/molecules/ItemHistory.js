// @flow
import { Field, Form } from "react-final-form";
import { format } from "date-fns";
import { graphql } from "react-apollo";
import { scheduledContactUpdate } from "../graphql/mutation/scheduledContact.graphql";
import Done from "../../../../ui/atoms/icons/Done";
import InputControl from "../../../../ui/atoms/misc/InputControl";
import React, { useState } from "react";
import Undone from "../../../../ui/atoms/icons/Undone";
import colors from "../../../../ui/colors";
import contactTypeFilter from "../../../../filters/contactType";
import styled from "@emotion/styled";
import type { scheduledContactSearch_scheduledContact_search } from "../../../../__generated__/scheduledContactSearch";

const ItemHistory = ({
  contact,
  scheduledContactUpdate,
  onChangeComment
}: {
  contact: scheduledContactSearch_scheduledContact_search,
  onChangeComment: Function,
  scheduledContactUpdate: Function
}) => {
  const [mode, setMode] = useState<string>();
  const date = new Date(contact.scheduledDate);
  let StatusIcon;

  if (contact.comment) {
    StatusIcon = Done;
  } else if (!contact.comment) {
    StatusIcon = Undone;
  }

  function handleSubmitComment(values) {
    return scheduledContactUpdate({
      variables: { id: contact.id, input: { comment: values.comment } }
    }).then(() => {
      setMode();
      onChangeComment();
    });
  }

  return (
    <Item>
      <Title titleSelected={contact.contactResult !== "SUCCESS"}>
        <Icon>
          <StatusIcon />
        </Icon>
        {contactTypeFilter(contact.contactType)}
      </Title>
      <Info>
        <InfoItem>{contact.subject}</InfoItem>
        <InfoItemSelected>{format(date, "DD.MM.YY")}</InfoItemSelected>
        <InfoItemSelected>{format(date, "hh:mm")}</InfoItemSelected>
        <InfoItem>Иван</InfoItem>
      </Info>
      {!contact.comment && mode !== "ADDCOMMENT" && (
        <AddCommentButton onClick={() => setMode("ADDCOMMENT")}>
          + Добавить комментарий
        </AddCommentButton>
      )}

      {mode === "ADDCOMMENT" ? (
        <Form
          onSubmit={handleSubmitComment}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Field component={InputControl} name="comment" />
            </form>
          )}
        />
      ) : (
        <Comment>{contact.comment}</Comment>
      )}
    </Item>
  );
};

export default graphql(scheduledContactUpdate, {
  name: "scheduledContactUpdate"
})(ItemHistory);

const Item = styled.div`
  border-bottom: solid 1px rgba(0, 0, 0, 0.1);
  padding: 2.5rem 0 2.2rem 4rem;
  margin-left: 3rem;
`;
const Title = styled.h4`
  font-size: 1.8rem;
  font-weight: 400;
  color: ${p => (p.titleSelected ? colors.deepLavender : colors.lightGreyBlue)};
  margin: 0 0 1.1rem;
  position: relative;
`;
const AddCommentButton = styled.div`
  display: inline-block;
  cursor: pointer;
  color: ${colors.lightGreyBlue};
  font-size: 1.4rem;
`;
const Comment = styled.div`
  font-size: 1.4rem;
`;
const Icon = styled.span`
  position: absolute;
  top: 50%;
  left: -4rem;
  transform: translateY(-50%);
  font-size: 0;
  line-height: 0;
`;
const Info = styled.div`
  display: flex;
  align-items: center;
  font-size: 1.4rem;
  margin: 0 0 2rem;
`;
const InfoItem = styled.div`
  color: ${colors.lightGreyBlue};
  margin-right: 3.7rem;

  &:last-of-type {
    margin: 0 0 0 auto;
  }
`;
const InfoItemSelected = styled(InfoItem)`
  color: ${colors.darkGrey};
`;
