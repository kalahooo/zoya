import { Form, Field } from "react-final-form";
import { Header, theme } from "../../../../ui";
import InputControl from "../../../../ui/atoms/misc/InputControl";
import React from "react";
import colors from "../../../../ui/colors";
import month from "./constants";
import styled from "@emotion/styled";

export default function({ onSubmit = fn => fn }) {
  return (
    <Block>
      <Block.Content>
        <Header.H4>Эквайринговый оборот Предприятия. Прошлый год</Header.H4>

        <Form
          onSubmit={onSubmit}
          render={({}) => (
            <Block.Row>
              {month.map(name => (
                <Block.Column>
                  <Field
                    component={InputControl}
                    placeholder={name}
                    name={`prev-month_${name}`}
                  />
                </Block.Column>
              ))}
            </Block.Row>
          )}
        />

        <Header.H4>Эквайринговый оборот Предприятия. Текущий год</Header.H4>

        <Form
          onSubmit={onSubmit}
          render={({}) => (
            <Block.Row>
              {month.map(name => (
                <Block.Column>
                  <Field
                    component={InputControl}
                    placeholder={name}
                    name={`current-month_${name}`}
                  />
                </Block.Column>
              ))}
            </Block.Row>
          )}
        />
      </Block.Content>

      <Block.Aside>
        <Avarage>Среднее</Avarage>
        <Avarage.Value>0</Avarage.Value>
      </Block.Aside>
    </Block>
  );
}

const Block = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 3.8rem 3rem 1.5rem;
  position: relative;
  display: flex;
  justify-content: space-between;
`;

Block.Content = styled.div`
  width: 87rem;
`;

Block.Aside = styled.div`
  text-align: right;
  align-self: flex-end;
  padding-bottom: 3.5rem;
`;

Block.Row = styled.div`
  margin: 3rem -1.5rem 0;
  display: flex;
  flex-wrap: wrap;

  &:last-of-type {
    margin-bottom: 0;
  }
`;

Block.Column = styled.div`
  padding: 0 1.5rem;
  width: 16.6666%;
  margin-bottom: 3.5rem;
`;

const Avarage = styled.div`
  font-size: 1rem;
  color: ${colors.lightGreyBlue};
  margin-bottom: 1.4rem;
  line-height: 1.4;
`;

Avarage.Value = styled.div`
  font-size: 2.4rem;
  line-height: ${33 / 24};
  font-weight: 700;
  color: ${theme.font.color};
`;
