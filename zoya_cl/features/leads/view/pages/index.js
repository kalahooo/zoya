import AccountTemplate from "../../../../ui/templates/Account";
import LeadCard from "../organisms/LeadCard";
import React from "react";

export default function LeadView({ id }) {
  return (
    <AccountTemplate>
      <LeadCard id={id} />
    </AccountTemplate>
  );
}

LeadView.getInitialProps = function(ctx) {
  return { id: ctx.query.id };
};
