// @flow
import { graphql } from "react-apollo";
import { scheduledContactSearch } from "../../graphql/query/scheduledContact.graphql";
import ItemHistory from "../../molecules/ItemHistory";
import React from "react";
import type { scheduledContactSearch_scheduledContact_search } from "../../../../../__generated__/scheduledContactSearch";

const LeadContacts = ({
  scheduledContactSearch
}: {
  scheduledContactSearch: Object
}) => {
  const contacts: scheduledContactSearch_scheduledContact_search =
    scheduledContactSearch?.scheduledContact?.search;

  if (scheduledContactSearch.loading && !contacts) {
    return <div>Загрузка взаимодействий</div>;
  }

  function handleChangeComment() {
    scheduledContactSearch.refetch();
  }

  return (
    <>
      {contacts.map(contact => (
        <ItemHistory
          contact={contact}
          key={contact.id}
          onChangeComment={handleChangeComment}
        />
      ))}
    </>
  );
};

export default graphql(scheduledContactSearch, {
  name: "scheduledContactSearch"
})(LeadContacts);
