import ErrorIcon from "../../../../../../ui/atoms/icons/ErrorIcon";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionContract = ({ lead }) => {
  return (
    <>
      <InfoGroup>
        <InfoGroupTitle>Банки</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Обслуживающий банк</InfoGroupColName>
            <InfoGroupColMain>ПАО «Сбербанк»</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Банк партнер</InfoGroupColName>
            <InfoGroupColMain> ПАО «БАНК УРАЛСИБ»</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Лицо, подписывающее договор</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Генеральный директор</InfoGroupColName>
            <InfoGroupColMain>Нет</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Фамилия</InfoGroupColName>
            <InfoGroupColMain>Иванов</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Имя</InfoGroupColName>
            <InfoGroupColMain>Иван</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Отчество</InfoGroupColName>
            <InfoGroupColMain>Иванович</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Должность</InfoGroupColName>
            <InfoGroupColMain>Исполнительный директор</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Основание полномочий</InfoGroupColName>
            <InfoGroupColMain>Доверенность</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Удостоверяющий документ</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Гражданство</InfoGroupColName>
            <InfoGroupColMain>РФ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Вид</InfoGroupColName>
            <InfoGroupColMain>Паспорт</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Серия и номер</InfoGroupColName>
            <InfoGroupColMain>0000 000000</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Кем выдан</InfoGroupColName>
            <InfoGroupColMain>УФМС Таганский г. Москва</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дата выдачи</InfoGroupColName>
            <InfoGroupColMain>ДД.ММ.ГГГГ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Место рождения</InfoGroupColName>
            <InfoGroupColMain>Москва</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дата рождения</InfoGroupColName>
            <InfoGroupColMain>ДД.ММ.ГГГГ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Адрес прописки</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Индекс</InfoGroupColName>
            <InfoGroupColMain>000000</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Страна</InfoGroupColName>
            <InfoGroupColMain>Российская Федерация</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Регион</InfoGroupColName>
            <InfoGroupColMain>Московская область</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Район</InfoGroupColName>
            <InfoGroupColMain>Москва</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Населенный пункт</InfoGroupColName>
            <InfoGroupColMain>г. Москва</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Улица</InfoGroupColName>
            <InfoGroupColMain>Таганская</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дом</InfoGroupColName>
            <InfoGroupColMain>20</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Корпус</InfoGroupColName>
            <InfoGroupColMain>1</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Строение</InfoGroupColName>
            <InfoGroupColMain>2</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Квартира/Офис</InfoGroupColName>
            <InfoGroupColMain>10</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Адрес проживания</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Индекс</InfoGroupColName>
            <InfoGroupColMain>000000</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Страна</InfoGroupColName>
            <InfoGroupColMain>Российская Федерация</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Регион</InfoGroupColName>
            <InfoGroupColMain>Московская область</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Район</InfoGroupColName>
            <InfoGroupColMain>Москва</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Населенный пункт</InfoGroupColName>
            <InfoGroupColMain>г. Москва</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Улица</InfoGroupColName>
            <InfoGroupColMain>Таганская</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дом</InfoGroupColName>
            <InfoGroupColMain>20</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Корпус</InfoGroupColName>
            <InfoGroupColMain>1</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Строение</InfoGroupColName>
            <InfoGroupColMain>2</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Квартира/Офис</InfoGroupColName>
            <InfoGroupColMain>10</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Адреса точек продаж</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Адрес 1</InfoGroupColName>
            <InfoGroupColMain>190190, Москва, Новый Арбат, 22</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Номер Договора Эквайринга</InfoGroupColName>
            <InfoGroupColMain>AA0-123-5678</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дата подписания</InfoGroupColName>
            <InfoGroupColMain>ДД.ММ.ГГГГ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <br />
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Адрес 2</InfoGroupColName>
            <InfoGroupColMain>190190, Москва, Новый Арбат, 22</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Номер Договора Эквайринга</InfoGroupColName>
            <InfoGroupColMain>AA0-123-5678</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дата подписания</InfoGroupColName>
            <InfoGroupColMain>ДД.ММ.ГГГГ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>
    </>
  );
};

export default SectionContract;

const InfoGroup = styled.div`
  margin: 0 0 1rem;
`;
const InfoGroupTitle = styled.h4`
  padding: 1rem 3rem;
  background: ${colors.iceBlue};
  margin: 0 0 1rem;
  font-size: 1.6rem;
  font-weight: 700;
  width: 90rem;
`;
const InfoGroupRow = styled.div`
  display: flex;
  padding: 0 3rem;
  width: 90rem;
`;
const InfoGroupColName = styled.div`
  width: 20rem;
  margin: 0 5rem;
  font-size: 1.6rem;
  line-height: 1.25;
  padding: 1rem 0;
  color: ${colors.lightGreyBlue};
`;
const InfoGroupColMain = styled.div`
  width: 34rem;
  font-size: 1.6rem;
  line-height: 1.25;
  padding: 1rem 0;
  text-transaform: uppercase;
`;
const InfoGroupAside = styled.div`
  position: absolute;
  left: 100%;
  font-size: 1.4rem;
  line-height: 20 / 14;
  top: 0;
  padding: 1rem 0;
  width: 32rem;
  margin-left: 7rem;
`;
const InfoGroupRowSelected = styled(InfoGroupRow)`
  background: #fef5f5;
`;
const TextSelected = styled.span`
  color: ${colors.orangeyRed};

  a {
    color: inherit;
  }
`;
const InfoGroupItem = styled.div`
  position: relative;
`;
const InfoGroupRowIcon = styled.div`
  position: absolute;
  left: 3rem;
  top: 0.9rem;
`;
