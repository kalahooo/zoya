import ErrorIcon from "../../../../../../ui/atoms/icons/ErrorIcon";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

const SectionConstituent = ({ lead }) => {
  return (
    <>
      <InfoGroup>
        <InfoGroupTitle>Свидетельство</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ОГРН</InfoGroupColName>
            <InfoGroupColMain>{lead.ogrn}</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Поставлена на учет</InfoGroupColName>
            <InfoGroupColMain>01.01.2010</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Налоговый орган</InfoGroupColName>
            <InfoGroupColMain>
              Инспекция Федеральной налоговой Службы #1 по г. Москве 7701
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRowSelected>
            <InfoGroupRowIcon>
              <ErrorIcon />
            </InfoGroupRowIcon>
            <InfoGroupColName>ИНН</InfoGroupColName>
            <InfoGroupColMain>{lead.inn}</InfoGroupColMain>
            <InfoGroupAside>
              <TextSelected>
                Необходима <a href="#">проверка данных</a>
              </TextSelected>
            </InfoGroupAside>
          </InfoGroupRowSelected>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>КПП</InfoGroupColName>
            <InfoGroupColMain>123456789</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Наименование юридического лица</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              Полное наименование юридического лица
            </InfoGroupColName>
            <InfoGroupColMain>
              ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ МИКРОКРЕДИТНАЯ КОМПАНИЯ
              "ЗОЯ ФИНАНС"
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Сокращенное наименование</InfoGroupColName>
            <InfoGroupColMain>ООО МКК "ЗОЯ ФИНАНС"</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ОГРН</InfoGroupColName>
            <InfoGroupColMain>1187746282932</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Включен в реестр</InfoGroupColName>
            <InfoGroupColMain>29.03.2019</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              ГРН и дата внесения в ЕГРЮЛ записи
            </InfoGroupColName>
            <InfoGroupColMain>8187747595588 18.04.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Адрес (место нахождения)</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Почтовый индекс</InfoGroupColName>
            <InfoGroupColMain>105082</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Субъект РФ</InfoGroupColName>
            <InfoGroupColMain>ГОРОД МОСКВА</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Улица</InfoGroupColName>
            <InfoGroupColMain> ПЕРЕУЛОК СПАРТАКОВСКИЙ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дом</InfoGroupColName>
            <InfoGroupColMain> ДОМ 2</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Корпус</InfoGroupColName>
            <InfoGroupColMain> СТРОЕНИЕ 1</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Офис</InfoGroupColName>
            <InfoGroupColMain> ПОМЕЩЕНИЕ Н.П.13</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              ГРН и дата внесения в ЕГРЮЛ записи
            </InfoGroupColName>
            <InfoGroupColMain>1187746282932 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Сведения о регистрации</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Способ образования</InfoGroupColName>
            <InfoGroupColMain>Создание юридического лица</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ОГРН</InfoGroupColName>
            <InfoGroupColMain>1187746282932</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName> Дата регистрации</InfoGroupColName>
            <InfoGroupColMain> 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              ГРН и дата внесения в ЕГРЮЛ записи
            </InfoGroupColName>
            <InfoGroupColMain>1187746282932 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>
          Сведения о регистрирующем органе по месту нахождения юридического лица
        </InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              Наименование регистрирующего органа
            </InfoGroupColName>
            <InfoGroupColMain>
              Межрайонная инспекция Федеральной налоговой службы № 46 по г.
              Москве{" "}
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Адрес регистрирующего органа</InfoGroupColName>
            <InfoGroupColMain>
              125373, г.Москва, Походный проезд, домовладение 3, стр.2
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              ГРН и дата внесения в ЕГРЮЛ записи
            </InfoGroupColName>
            <InfoGroupColMain>1187746282932 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>Сведения об учете в налоговом органе</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ИНН записи </InfoGroupColName>
            <InfoGroupColMain>9701102857</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>КПП</InfoGroupColName>
            <InfoGroupColMain>770101001</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName> Дата постановки на учет</InfoGroupColName>
            <InfoGroupColMain>15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Наименование налогового органа</InfoGroupColName>
            <InfoGroupColMain>
              Инспекция Федеральной налоговой службы № 1 по г.Москве
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ГРН и дата внесения в ЕГРЮЛ</InfoGroupColName>
            <InfoGroupColMain>2187747335004 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>
          Сведения о регистрации в качестве страхователя в территориальном
          органе Пенсионного фонда Российской Федерации
        </InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Регистрационный номер</InfoGroupColName>
            <InfoGroupColMain>087108154427</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дата регистрации</InfoGroupColName>
            <InfoGroupColMain>20.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              Наименование территориального органа Пенсионного фонда
            </InfoGroupColName>
            <InfoGroupColMain>
              Государственное учреждение - Главное Управление Пенсионного фонда
              РФ №10 Управление №2 по г. Москве и Московской области
              муниципальный район Басманное г.Москвы
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ГРН и дата внесения в ЕГРЮЛ</InfoGroupColName>
            <InfoGroupColMain>2187747335004 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>
          Сведения о регистрации в качестве страхователя в исполнительном органе
          Фонда социального страхования Российской Федерации
        </InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Регистрационный номер</InfoGroupColName>
            <InfoGroupColMain>087108154427</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Дата регистрации</InfoGroupColName>
            <InfoGroupColMain>20.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>
              Наименование территориального органа Пенсионного фонда
            </InfoGroupColName>
            <InfoGroupColMain>
              Филиал №6 Государственного учреждения - Московского регионального
              отделения Фонда социального страхования Российской Федерации
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ГРН и дата внесения в ЕГРЮЛ</InfoGroupColName>
            <InfoGroupColMain>2187747335004 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroup>
        <InfoGroupTitle>
          Сведения об уставном капитале (складочном капитале, уставном фонде,
          паевых взносах)
        </InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Вид</InfoGroupColName>
            <InfoGroupColMain>УСТАВНЫЙ КАПИТАЛ</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Размер (в рублях)</InfoGroupColName>
            <InfoGroupColMain>10000</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ГРН и дата внесения в ЕГРЮЛ</InfoGroupColName>
            <InfoGroupColMain>2187747335004 15.03.2018</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>
    </>
  );
};

export default SectionConstituent;

const InfoGroup = styled.div`
  margin: 0 0 1rem;
`;
const InfoGroupTitle = styled.h4`
  padding: 1rem 3rem;
  background: ${colors.iceBlue};
  margin: 0 0 1rem;
  font-size: 1.6rem;
  font-weight: 700;
  width: 90rem;
`;
const InfoGroupRow = styled.div`
  display: flex;
  padding: 0 3rem;
  width: 90rem;
`;
const InfoGroupColName = styled.div`
  width: 20rem;
  margin: 0 5rem;
  font-size: 1.6rem;
  line-height: 1.25;
  padding: 1rem 0;
  color: ${colors.lightGreyBlue};
`;
const InfoGroupColMain = styled.div`
  width: 34rem;
  font-size: 1.6rem;
  line-height: 1.25;
  padding: 1rem 0;
  text-transaform: uppercase;
`;
const InfoGroupAside = styled.div`
  position: absolute;
  left: 100%;
  font-size: 1.4rem;
  line-height: 20 / 14;
  top: 0;
  padding: 1rem 0;
  width: 32rem;
  margin-left: 7rem;
`;
const InfoGroupRowSelected = styled(InfoGroupRow)`
  background: #fef5f5;
`;
const TextSelected = styled.span`
  color: ${colors.orangeyRed};

  a {
    color: inherit;
  }
`;
const InfoGroupItem = styled.div`
  position: relative;
`;
const InfoGroupRowIcon = styled.div`
  position: absolute;
  left: 3rem;
  top: 0.9rem;
`;
