import { Field, Form } from "react-final-form";
import { theme } from "../../../../../../ui";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

export default ({ onSubmit = fn => fn }) => (
  <Block>
    <Table>
      <Table.Row>
        <Table.HeadColumn>Срок аванса</Table.HeadColumn>
        <Table.HeadColumn>Сумма займа</Table.HeadColumn>
        <Table.HeadColumn>Процентная ставка</Table.HeadColumn>
        <Table.HeadColumn>Сумма процентов</Table.HeadColumn>
      </Table.Row>

      <Table.Row>
        <Table.Column>5</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>32 200</Table.Column>
      </Table.Row>

      <Table.Row>
        <Table.Column>6</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>32 200</Table.Column>
      </Table.Row>

      <Table.Row>
        <Table.Column>7</Table.Column>
        <Table.Column>322 000</Table.Column>
        <Table.Column>16.5%</Table.Column>
        <Table.Column>32 200</Table.Column>
      </Table.Row>
    </Table>

    <Form
      onSubmit={onSubmit}
      render={({}) => (
        <Retention>
          <Field
            component={InputControl}
            name="retention"
            placeholder="Удержание"
            size="big"
          />
        </Retention>
      )}
    />
  </Block>
);

const Block = styled.div`
  width: calc(50% - 15px);
  border: 1px solid ${colors.veryLightBlue};
  padding: 1rem 0 3rem;
`;

const Table = styled.div`
  margin-bottom: 4.8rem;
`;

Table.Row = styled.div`
  display: flex;
  padding-left: 3rem;
  height: 4rem;

  &:nth-of-type(even) {
    background-color: ${colors.iceBlue};
  }
`;

Table.Column = styled.div`
  width: 33.3333%;
  font-size: 1.6rem;
  color: ${theme.font.color};
  line-height: 4rem;
`;

Table.HeadColumn = styled(Table.Column)`
  font-size: 1rem;
  color: ${colors.lightGreyBlue};
`;

const Retention = styled.div`
  width: 12rem;
  padding-left: 3rem;
`;
