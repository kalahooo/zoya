import { Field, Form } from "react-final-form";
import { Header } from "../../../../../../ui";
import InputControl from "../../../../../../ui/atoms/misc/InputControl";
import React from "react";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";

export default function Information({ lead }) {
  return (
    <Form
      initialValues={lead}
      onSubmit={() => void 0}
      render={({}) => (
        <InformationWrap>
          <Header.H4>Контактная информация</Header.H4>
          <FormRows>
            <FormRowFlex>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled
                  name="lastName"
                  placeholder="Фамилия"
                />
              </FormColumn>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled
                  name="firstName"
                  placeholder="Имя"
                />
              </FormColumn>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled
                  name="patronymicName"
                  placeholder="Отчество"
                />
              </FormColumn>
            </FormRowFlex>
            <FormRowFlex>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled
                  name="phone"
                  placeholder="Телефон"
                />
              </FormColumn>
              <FormColumn>
                <Field
                  component={InputControl}
                  disabled
                  name="email"
                  placeholder="email"
                />
              </FormColumn>
            </FormRowFlex>
          </FormRows>
        </InformationWrap>
      )}
    />
  );
}

const InformationWrap = styled.div`
  margin: 0 0 4rem;
  background: ${colors.iceBlue};
  padding: 3.8rem 3rem 3.4rem;
`;
const Details = styled.div`
  padding: 3.8rem 3rem 3.4rem;
  margin: 9rem 0 4rem;
`;
const FormRows = styled.div`
  width: 87rem;
`;
const FormRowWide = styled.div`
  margin: 3rem 0 0;
`;
const FormRowFlex = styled.div`
  margin: 3rem -1.5rem 0;
  display: flex;

  &:last-of-type {
    margin-bottom: 0;
  }
`;
const FormColumn = styled.div`
  padding: 0 1.5rem;
  width: 33.3333%;
`;
const FormColumnSmall = styled.div`
  padding: 0 12rem 0 1.5rem;
  width: 33.3333%;
`;
