// @flow
import { ItemHistory } from "../../../molecules";
import Done from "../../../../../../ui/atoms/icons/Done";
import ErrorIcon from "../../../../../../ui/atoms/icons/ErrorIcon";
import Information from "./Information";
import LeadContacts from "../../LeadContacts";
import React from "react";
import Undone from "../../../../../../ui/atoms/icons/Undone";
import colors from "../../../../../../ui/colors";
import styled from "@emotion/styled";
import type { leadGet_lead_get } from "../../../../../../__generated__/leadGet";

const SectionMain = ({ lead }: { lead: leadGet_lead_get }) => {
  return (
    <>
      <Information lead={lead} />

      <InfoGroup>
        <Details>
          <DetailsTitle>Сумма займа</DetailsTitle>
          <DetailsValue>
            {/*<DetailsIcon>
              <ErrorIcon />
            </DetailsIcon>*/}
            {lead.requestedLoanAmount || 0}
            {/*<InfoGroupAside>
              <TextSelected>Выход за границы</TextSelected>
            </InfoGroupAside>*/}
          </DetailsValue>
          <DetailsTitle>Срок займа</DetailsTitle>
          <DetailsValue>{lead.requestedLoanTerm} мес</DetailsValue>
        </Details>
        <InfoGroupTitle>Основная информация</InfoGroupTitle>
        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Краткое наименование</InfoGroupColName>
            <InfoGroupColMain>{lead.companyName}</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>

        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>ИНН</InfoGroupColName>
            <InfoGroupColMain>{lead.inn}</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>

        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupRowIcon>
              {lead.isFromExternalSource ? <Done /> : <ErrorIcon />}
            </InfoGroupRowIcon>
            <InfoGroupColName>
              Регистрационные данных получены из внешнего источника
            </InfoGroupColName>
            <InfoGroupColMain>
              {lead.isFromExternalSource ? "Да" : "Нет"}
            </InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>

        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Запрошенная сумма</InfoGroupColName>
            <InfoGroupColMain>{lead.requestedLoanAmount}</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>

        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Запрошенный срок</InfoGroupColName>
            <InfoGroupColMain>{lead.requestedLoanAmount} мес</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>

        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Регион</InfoGroupColName>
            <InfoGroupColMain>{lead.region}</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>

        <InfoGroupItem>
          <InfoGroupRow>
            <InfoGroupColName>Город</InfoGroupColName>
            <InfoGroupColMain>{lead.city}</InfoGroupColMain>
          </InfoGroupRow>
        </InfoGroupItem>
      </InfoGroup>

      <InfoGroupTitle>История взаимодействий</InfoGroupTitle>

      <LeadContacts leadId={lead.id} />
    </>
  );
};

export default SectionMain;

const Details = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;
const DetailsTitle = styled.div`
  font-size: 1.4rem;
  color: ${colors.lightGreyBlue};
`;
const DetailsValue = styled.div`
  font-size: 2.4rem;
  font-weight: 700;
  margin: 0 0 3.4rem;
  lin-height: ${33 / 24};
  position: relative;
`;
const DetailsIcon = styled.div`
  position: absolute;
  top: 50%;
  left: -6rem;
  transform: translateY(-50%);
  font-size: 0;
  line-height: 0;
`;
const InfoGroup = styled.div`
  margin: 0 0 3rem;
  position: relative;
`;
const InfoGroupTitle = styled.h4`
  height: 4rem;
  padding: 0 3rem;
  line-height: 4rem;
  margin: 0 0 1rem;
  font-size: 1.6rem;
  font-weight: 700;
  width: 90rem;
`;
const InfoGroupRow = styled.div`
  display: flex;
  padding: 0 3rem;
  width: 90rem;
`;
const InfoGroupColName = styled.div`
  width: 20rem;
  margin: 0 5rem;
  font-size: 1.6rem;
  line-height: 1.25;
  padding: 1rem 0;
  color: ${colors.lightGreyBlue};
`;
const InfoGroupColMain = styled.div`
  width: 34rem;
  font-size: 1.6rem;
  line-height: 1.25;
  padding: 1rem 0;
  text-transaform: uppercase;
`;
const InfoGroupAside = styled.div`
  position: absolute;
  left: 100%;
  font-size: 1.4rem;
  line-height: 20 / 14;
  top: 0;
  padding: 1rem 0;
  width: 32rem;
  margin-left: 7rem;
  font-weight: 400;
`;
const InfoGroupRowSelected = styled(InfoGroupRow)`
  background: #fef5f5;
`;
const TextSelected = styled.span`
  color: ${colors.orangeyRed};

  a {
    color: inherit;
  }
`;
const InfoGroupItem = styled.div`
  position: relative;
`;
const InfoGroupRowIcon = styled.div`
  position: absolute;
  left: 3rem;
  top: 0.9rem;
`;
