// @flow
import { ButtonPrimary } from "../../../../../ui/atoms/buttons";
import { Header } from "../../../../../ui/atoms/typography";
import { css } from "@emotion/core";
import { graphql } from "react-apollo";
import { leadGet } from "../../graphql/query/leads.graphql";
import { scheduledContactSearch } from "../../graphql/query/scheduledContact.graphql";
import AddTaskModal from "../../../../../ui/organisms/AddTaskModal";
import Aside from "../../../../../ui/atoms/blocks/Aside";
import Crumbs from "../../../../../ui/atoms/misc/Crumbs";
import Edit from "../../../../../ui/atoms/icons/Edit";
import Head from "../../../../../ui/atoms/blocks/Head";
import Link from "next/link";
import Pie from "../../../../../ui/atoms/misc/Pie";
import Plus from "../../../../../ui/atoms/icons/Plus";
import React, { useState } from "react";
import Sections from "../sections";
import colors from "../../../../../ui/colors";
import styled from "@emotion/styled";

const LeadCard = ({ leadGet, id }) => {
  const [taskModalVisible, seTtaskModalVisiblity] = useState();

  if (!leadGet) {
    return <div>Error</div>;
  }

  if (leadGet.loading) {
    return (
      <div
        css={css`
          padding-left: 3rem;
          padding-top: 6rem;
        `}
      >
        Загрузка...
      </div>
    );
  }

  const lead = leadGet.lead?.get || {};

  function handleAddContact(v) {
    seTtaskModalVisiblity();
  }

  return (
    <>
      {taskModalVisible && (
        <AddTaskModal
          leadId={id}
          onCancel={() => seTtaskModalVisiblity()}
          onSuccess={handleAddContact}
          refetchQueries={[
            { query: scheduledContactSearch, variables: { leadId: lead.id } }
          ]}
        />
      )}
      <Layout>
        <Main>
          <Crumbs>
            <Crumbs.CrumbsLink href="/account">Рабочий стол</Crumbs.CrumbsLink>
            <Crumbs.CrumbsLink href="/account/leads">Лиды</Crumbs.CrumbsLink>
            <Crumbs.CrumbsCurrent>{lead.companyName}</Crumbs.CrumbsCurrent>
          </Crumbs>
          <Head>
            <Header.H2>{lead.companyName}</Header.H2>
            <ActionIcons>
              <Link href={`/account/leads/edit?id=${lead.id}`} passHref>
                <a>
                  <Edit />
                </a>
              </Link>
              <span onClick={() => seTtaskModalVisiblity(true)}>
                <Plus />
              </span>
            </ActionIcons>
            <HeadAside>
              <Lights>
                <Light color="green" />
                <Light color="yellow" />
                <Light color="gray" />
              </Lights>
              <Percent>
                <Pie value="25" />
                <PercentValue>25%</PercentValue>
              </Percent>
            </HeadAside>
          </Head>

          <Sections lead={lead} />
        </Main>

        <Aside>
          <Aside.Group>
            <ButtonsGroup>
              <ButtonPrimary disabled>Отправить на андеррайтинг</ButtonPrimary>
            </ButtonsGroup>
          </Aside.Group>

          <Aside.Group>
            <InfoCreate>
              <InfoCreateRow>
                <InfoCreateRowName>Создано:</InfoCreateRowName>
                {` `}
                12.12.2019 12:30
              </InfoCreateRow>
              <InfoCreateRow>
                <InfoCreateRowName>Пользователь:</InfoCreateRowName>
                {` `}
                Иванов Иван
              </InfoCreateRow>
            </InfoCreate>

            <InfoCreate>
              <InfoCreateRow>
                <InfoCreateRowName>Последнее изменение:</InfoCreateRowName>
                {` `}
                31.12.2019 17:00
              </InfoCreateRow>
              <InfoCreateRow>
                <InfoCreateRowName>Пользователь:</InfoCreateRowName>
                {` `}
                Петров Петр
              </InfoCreateRow>
            </InfoCreate>
          </Aside.Group>
        </Aside>
      </Layout>
    </>
  );
};

export default graphql(leadGet, { name: "leadGet" })(LeadCard);

const ActionIcons = styled.div`
  margin: 0 0 0 5rem;

  > * {
    margin-right: 2rem;
    cursor: pointer;
  }
`;
const InfoCreate = styled.div`
  color: ${colors.lightGreyBlue};
  margin: 0 0 1em;
`;
const InfoCreateRow = styled.div``;
const InfoCreateRowName = styled.strong``;
const Layout = styled.div`
  display: flex;
  min-height: calc(100vh - 7rem);
  background: #fff;
  width: 100%;
`;
const Main = styled.div`
  flex: 1;
  padding-right: 3rem;
`;
const HeadAside = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`;
const ButtonsGroup = styled.div`
  display: flex;
  flex-direction: column;
  width: 23rem;
  margin: 0 0 9rem;

  > * {
    padding: 0 !important;
  }
  > :not(:first-of-type) {
    margin-top: 2rem;
  }
`;
const Lights = styled.div`
  margin-left: auto;
  display: flex;
`;
const Light = styled.div`
  width: 1.6rem;
  height: 1.6rem;
  border-radius: 50%;
  margin-left: 0.5rem;
  background: ${({ color }) => {
    if (color === "green") {
      return colors.aquaMarine;
    } else if (color === "yellow") {
      return colors.macaroniAndCheese;
    } else if (color === "gray") {
      return colors.veryLightBlue;
    }
    return "black";
  }};
`;
const Percent = styled.div`
  margin-left: 4rem;
`;
const PercentValue = styled.div`
  font-weight: bold;
  color: ${colors.deepLavender};
  font-size: 1.1em;
  display: inline-block;
  vertical-align: middle;
`;
