import { Header, LinkDetails } from "../../../ui/atoms/typography";
import AccountTemplate from "../../../ui/templates/Account";
import Crumbs from "../../../ui/atoms/misc/Crumbs";
import Head from "../../../ui/atoms/blocks/Head";
import Link from "next/link";
import React from "react";
import Welcome from "../atoms/Welcome";
import styled from "@emotion/styled";
import useAuth from "../../../core/Auth/useAuth";

const Tmp = styled.div`
  padding-left: 3rem;
`;
const Account = () => {
  const [auth] = useAuth();

  return (
    <AccountTemplate>
      <div>
        <Welcome name={auth?.firstName} />
        <Tmp>
          <LinkDetails>
            <Link href="/auth/logout" passHref>
              <a>Выход</a>
            </Link>
          </LinkDetails>
        </Tmp>
      </div>
    </AccountTemplate>
  );
};

export default Account;
