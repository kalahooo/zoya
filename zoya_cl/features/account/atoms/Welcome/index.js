import { Header } from "../../../../ui";
import ArrowUp from "../../../../ui/atoms/icons/ArrowUp";
import Image from "./photo-start.jpg";
import React, { useState } from "react";
import styled from "@emotion/styled";

const Welcome = ({ name }) => {
  const [welcome, toggleWelcome] = useState(true);

  return (
    <WelcomeBlock
      isOpened={welcome}
      style={{ backgroundImage: `url(${Image})` }}
    >
      <Title>Добро пожаловать, {name}!</Title>
      <Button onClick={() => toggleWelcome(false)}>
        <ArrowUp />
      </Button>
    </WelcomeBlock>
  );
};

const WelcomeBlock = styled.div`
  height: ${p => (p.isOpened ? "46rem" : 0)};
  transition: height 0.6s;
  position: relative;
  overflow: hidden;
  background-size: cover;
  background-position: 50% 50%;
  background-repeat: no-repeat;
  margin-left: -3rem;
  z-index: 10;
`;
const Title = styled(Header.H1)`
  color: #fff;
  position: absolute;
  left: 7rem;
  bottom: 5.2rem;
  margin: 0;
`;
const Button = styled.div`
  position: absolute;
  left: 146rem;
  bottom: 6.2rem;
  width: 4.6rem;
  height: 4.6rem;
  background: #fff;
  cursor: pointer;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default Welcome;
