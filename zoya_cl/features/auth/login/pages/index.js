import LoginForm from "../ogranisms";
import React from "react";
import styled from "@emotion/styled";

const LoginWrap = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <LoginWrap>
    <LoginForm />
  </LoginWrap>
);
