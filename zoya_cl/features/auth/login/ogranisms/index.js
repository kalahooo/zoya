import { FORM_ERROR } from "final-form";
import { Field, Form } from "react-final-form";
import ButtonPrimary from "../../../../ui/atoms/buttons/ButtonPrimary";
import Cookies from "js-cookie";
import InputControl from "../../../../ui/atoms/misc/InputControl";
import Modal from "../../../../ui/atoms/misc/Modal";
import React from "react";
import Router from "next/router";
import styled from "@emotion/styled";
import useAuth from "../../../../core/Auth/useAuth";

export const tmpUser = {
  firstName: "Иван",
  lastName: "Иванов"
};

const LoginForm = () => {
  const [auth, setAuth] = useAuth();

  function handleSubmitForm(values) {
    return new Promise(resolve => {
      setTimeout(() => {
        if (values.email === "admin@admin.com" && values.password === "12") {
          setAuth(tmpUser);

          Cookies.set("token", "awerdsf12df");
          Router.replace("/account");
          // const pPath = sessionStorage.getItem("pPath", Router.route);

          // if (pPath) {
          //   // sessionStorage.removeItem("pPath");
          //   Router.replace(pPath);
          // } else {
          //   Router.replace("/account");
          // }
        } else {
          return resolve({ [FORM_ERROR]: "Пользователь не найден" });
        }
      }, 1000);
    });
  }

  return (
    <Modal>
      <Form
        onSubmit={handleSubmitForm}
        render={({ handleSubmit, submitError, submitting }) => (
          <TmpForm onSubmit={handleSubmit}>
            <Modal.SubTitle>Вход</Modal.SubTitle>

            <Field
              autocomplete="off"
              component={InputControl}
              name="email"
              placeholder="Email"
              type="email"
            />

            <Field
              component={InputControl}
              name="password"
              placeholder="Пароль"
              type="password"
            />

            {submitError && <p>{submitError}</p>}

            <ButtonPrimary disabled={submitting}>Вход</ButtonPrimary>
          </TmpForm>
        )}
      />
    </Modal>
  );
};

export default LoginForm;

const TmpForm = styled.form`
  text-align: center;
  label {
    display: block;
    text-align: left;
  }
  input {
    display: block;
    text-align: left;
  }
  & > * {
    margin-bottom: 4rem;
    &:last-child {
      margin-bottom: 0;
    }
  }
`;
