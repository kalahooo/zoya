export default function GenerateCalendar(ms) {
  const dateMin = new Date();
  const date = new Date(ms);
  let currentDate = new Date(date.getFullYear(), date.getMonth());
  const currentMonth = currentDate.getMonth();
  const result = [];

  while (currentDate.getMonth() === currentMonth) {
    const cDay = currentDate.getDay(),
      cDate = currentDate.getDate(),
      offsetDate =
        (cDay % 7) - 1 < 0
          ? cDate - ((cDay % 7) + 6)
          : cDate - ((cDay % 7) - 1);
    currentDate.setDate(offsetDate);

    for (let i = 7; i > 0; i--) {
      const isDisabled =
        currentDate.getFullYear() < dateMin.getFullYear() ||
        (currentDate.getFullYear() === dateMin.getFullYear() &&
          currentDate.getMonth() < dateMin.getMonth()) ||
        (currentDate.getFullYear() === dateMin.getFullYear() &&
          currentDate.getMonth() === dateMin.getMonth() &&
          currentDate.getDate() < dateMin.getDate());

      result.push({
        day: currentDate.getDate(),
        isDisabled,
        isWeekEnd: i < 3,
        date: new Date(currentDate).getTime()
      });

      currentDate.setDate(currentDate.getDate() + 1);
    }
  }

  return result;
}
