export const required = (msg = "Обязательное поле") => value =>
  value ? void 0 : msg;

export const isInnOgrn = v => {
  const re = /^([0-9]{10}|[0-9]{12}|[0-9]{13}|[0-9]{15})$/;

  return re.test(v);
};

export const isNumeric = n => {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

export const validEmail = (msg = "Некорректный email") => value =>
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    value
  )
    ? void 0
    : msg;
