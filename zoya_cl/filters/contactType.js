export default function contactTypeFilter(value) {
  switch (value) {
    case "EMAIL":
      return "Отправить e-mail";
    case "MEETING":
      return "Назначить встречу";
    case "PHONECALL":
      return "Телефонный звонок";
    default:
      return value;
  }
}
