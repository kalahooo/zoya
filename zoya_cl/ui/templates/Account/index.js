import Header from "../../molecules/Header";
import React from "react";
import Sidebar from "../../molecules/Sidebar";
import styled from "@emotion/styled";

export default function AccountTemplate({ children }) {
  return (
    <Template>
      <Sidebar />
      <Content>
        <Header>Header</Header>
        <Main>{children}</Main>
      </Content>
    </Template>
  );
}

const Template = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
`;

const Main = styled.main`
  position: absolute;
  left: 0;
  right: 0;
  top: 7rem;
  bottom: 0;
  overflow: auto;
  padding: 0 0 3rem 3rem;
`;
const Content = styled.div`
  flex: 1;
  position: relative;
`;
