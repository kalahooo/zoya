const colors = {
  darkGrey: "#212224",
  veryLightPink: "#eaeaea",
  charcoalGrey: "#474b4f",
  lightBlueGrey: "#c6c9cf",
  lightGreyBlue: "#aaaeb3",
  battleshipGrey: "#6f737a",
  charcoalGrey_2: "#393c40",
  orangeyRed: "#f44236",
  aquaMarine: "#1de9b6",
  palePurple: "#a389d4",
  macaroniAndCheese: "#f4c22b",
  veryLightBlue: "#ecedef",
  deepLavender: "#874bc7",
  purply: "#7944b3",
  purply_2: "#6f3ea3",
  azure: "#04a9f5",
  iceBlue: "#f9fafa",
  white: "#fff"
};

export default colors;
