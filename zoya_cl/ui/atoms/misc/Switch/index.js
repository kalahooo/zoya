import React from "react";
import colors from "../../../colors";
import styled from "@emotion/styled";

const Switch = ({ items = [], input: { value, onChange } }) => {
  return (
    <SwitchWrap>
      {items.map(item => (
        <Item
          active={value === item.value}
          key={item.value}
          onClick={() => onChange(item.value)}
        >
          {item.label}
        </Item>
      ))}
    </SwitchWrap>
  );
};

export default Switch;

const SwitchWrap = styled.div`
  display: flex;
`;

const Item = styled.div`
  margin-right: 4rem;
  font-weight: bold;
  cursor: pointer;
  color: ${({ active }) =>
    active ? colors.deepLavender : colors.lightGreyBlue};
`;
