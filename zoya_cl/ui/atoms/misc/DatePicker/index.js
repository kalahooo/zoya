import React, { useState } from "react";
import colors from "../../../colors";
import styled from "@emotion/styled";
import GenerateCalendar from "../../../../utils/calendar";
import Day from "../../icons/Day";
import Sun from "../../icons/Sun";
import Next from "../../icons/Next";

export default class DatePicker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isDatePickerOpened: false,
      datepickerDate: new Date(props.input.value).getTime() || new Date().getTime()
    }
    this.monthNames = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'];
    this.fullMonthNames = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
    this.daysWeek = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
  }

  getValue = () => {
    const date = this.props.input.value ? new Date(this.props.input.value) : new Date();

    return {
      day: date.getDay(),
      date: date.getDate(),
      month: this.monthNames[date.getMonth()],
      fullMonth: this.fullMonthNames[date.getMonth()],
      year: date.getFullYear()
    };
  };

  setFixedDate = period => {
    const date = new Date();

    this.saveDate(new Date(date.getFullYear(), date.getMonth(), date.getDate() + period).getTime());
  }

  saveDate = (date, isDisabled) => {
    if (isDisabled) {
      return;
    }

    this.props.input.onChange(date);
    this.toggleDatePicker(false);
  };

  isPrevDisabled = () => {
    const dateC = new Date();
    const date = new Date(this.state.datepickerDate);
    return dateC.getFullYear() === date.getFullYear() && dateC.getMonth() === date.getMonth();
  }

  isChosenDate = date => {
    const dateInput = new Date(this.props.input.value);
    const dateC = new Date(date);

    return dateC.getFullYear() === dateInput.getFullYear() && dateC.getMonth() === dateInput.getMonth() && dateC.getDate() === dateInput.getDate();
  }

  handlePrev = () => {
    if (this.isPrevDisabled()) {
      return;
    }

    const date = new Date(this.state.datepickerDate);

    this.setState({
      datepickerDate: new Date(date.getFullYear(), date.getMonth() - 1)
    });
  };

  handleNext = () => {
    const date = new Date(this.state.datepickerDate);

    this.setState({
      datepickerDate: new Date(date.getFullYear(), date.getMonth() + 1)
    });
  };

  toggleDatePicker = isDatePickerOpened => {
    this.setState({
      isDatePickerOpened
    });
  }

  render() {
    const {
      input,
      placeholder,
      notShrink
    } = this.props;
    const dayNow = new Date().getDay();

    return (
      <Control>
        {placeholder && !(input.value && notShrink) && (
          <Label htmlFor={input.name} isSmall={input.value || this.state.isDatePickerOpened}>
            {placeholder}
          </Label>
        )}
        <Input
          tabIndex="1"
          onFocus={() => this.toggleDatePicker(true)}
          onClick={() => this.toggleDatePicker(true)}
        >
          {
            input.value ? (
              <>
                {`${this.getValue().date} ${this.getValue().fullMonth}`}
              </>
            ) : <>&nbsp;</>
          }
        </Input>
        
        <ButtonSelect onClick={() => this.toggleDatePicker(true)}>Выбрать дату</ButtonSelect>

        {
          this.state.isDatePickerOpened && (
            <DateContainer onMouseLeave={() => this.toggleDatePicker(false)}>
              <CurrentDate>{`${this.getValue().date} ${this.getValue().month}`}</CurrentDate>
              <DateRow onClick={() => this.setFixedDate(0)}>
                <Day />
                <span>Сегодня</span>
                <DayName>{this.daysWeek[dayNow]}</DayName>
              </DateRow>
              <DateRow onClick={() => this.setFixedDate(1)}>
                <Sun />
                <span>Завтра</span>
                <DayName>{this.daysWeek[(dayNow + 1) % 7]}</DayName>
              </DateRow>
              <DateRow onClick={() => this.setFixedDate(7)}>
                <Next />
                <span>Через неделю</span>
                <DayName>{this.daysWeek[dayNow]}</DayName>
              </DateRow>
              <HeadDate>
                <HeadTitle>
                  {`${this.monthNames[new Date(this.state.datepickerDate).getMonth()]}. ${new Date(this.state.datepickerDate).getFullYear()}`}
                </HeadTitle>
                <DateControls>
                  <DatePrev isDisabled={this.isPrevDisabled()} onClick={this.handlePrev} />
                  <Round />
                  <DateNext onClick={this.handleNext} />
                </DateControls>
              </HeadDate>
              <Days>
                {
                  ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'].map(day => (
                    <DayWeek key={day}>{day}</DayWeek>
                  ))
                }
              </Days>
              <Month>
                {
                  GenerateCalendar(this.state.datepickerDate, this.props.input.value).map(({ day, isDisabled, isWeekEnd, date }) => (
                    <MonthDay
                      onClick={() => this.saveDate(date, isDisabled)}
                      key={date}
                      isDisabled={isDisabled}
                      isWeekEnd={isWeekEnd}
                      isActive={this.isChosenDate(date)}
                    >{day}</MonthDay>
                  ))
                }
              </Month>
            </DateContainer>
          )
        }
        
      </Control>
    )
  }
}

const ButtonSelect = styled.div`
  color: ${colors.lightGreyBlue};
  cursor: pointer;
  position: absolute;
  left: calc(100% + 2rem);
  bottom: 0;
  white-space: nowrap;
  line-height: 3.4rem;
`;
const Month = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 0 .7rem;
  justify-content: space-between;
`;
const MonthDay = styled.div`
  width: 3.4rem;
  height: 3.4rem;
  line-height: 3.4rem;
  text-align: center;
  background: ${p => p.isActive ? colors.deepLavender : "white"};
  border-radius: .8rem;
  margin: 0 .3rem;
  ${p => {
    let result = `cursor: pointer`;
    if (p.isDisabled) {
      result = `
        color: ${colors.veryLightPink};
        cursor: default;
      `;
    } else if (p.isWeekEnd) {
      result = `
        color: ${colors.lightGreyBlue};
        cursor: pointer;
      `;
    }
    if (p.isActive) {
      result = `
        cursor: pointer;
        color: #fff;
        font-weight: 700;
        background: ${colors.deepLavender};
      `;
    }
    return result;
  }}
`;
const Days = styled.div`
  display: flex;
  padding: 0 1rem;
  margin: 0 0 1.4rem;
`;
const DayWeek = styled.div`
  text-transform: uppercase;
  text-align: center;
  width: ${100 / 7}%;
  font-size: 1.4rem;
  color: ${colors.lightGreyBlue};
`;
const DateControls = styled.div`
  display: flex;
  align-items: center;
`;
const HeadTitle = styled.div`
  font-weight: 700;
`;
const DateControl = styled.div`
  width: 3rem;
  height: 3rem;
  cursor: ${p => p.isDisabled ? "default" : "pointer"};
  color: ${p => p.isDisabled ? "rgba(0, 0, 0, .1)" : colors.deepLavender};
  display: flex;
  align-items: center;
  justify-content: center;

  &::before {
    content: "";
    width: .8rem;
    height: .8rem;
    border-left: solid .2rem;
    border-bottom: solid .2rem;
  }
`;
const DatePrev = styled(DateControl)`
  &::before {
    transform: rotate(45deg);
  }
`;
const DateNext = styled(DateControl)`
  &::before {
    transform: rotate(-135deg);
  }
`;
const Round = styled.div`
  width: 1rem;
  height: 1rem;
  margin: 0 1rem;
  border-radius: 1rem;
  border: solid 1px ${colors.lightGreyBlue};
`;
const HeadDate = styled.div`
  border-top: solid 2px rgba(0, 0, 0, .1);
  padding: 1.3rem 2rem .6rem 2.2rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 4.9rem;
`;
const Control = styled.div`
  position: relative;
`;
const Label = styled.label`
  font-size: ${p => (p.isSmall ? "1rem" : "1.6rem")};
  color: ${colors.lightGreyBlue};
  transition: all 0.25s;
  transform: ${p =>
    p.isSmall
      ? `translateY(-2rem)`
      : "translateY(0)"};
  position: absolute;
  height: 2rem;
  line-height: 2rem;
  top: 0;
  left: 0;
  bottom: 0;
  margin: auto 0;
  white-space: nowrap;
`;
const Input = styled.div`
  width: 100%;
  display: block;
  border: 0;
  font-size: 1.6rem};
  font-weight: 400;
  line-height: 3.4rem;
  font-family: inherit;
  color: ${colors.darkGrey};
  background: transparent;
  position: relative;
  outline: none;
  border-bottom: solid 1px ${colors.lightBlueGrey};
  transition: border-bottom 0.1s ease-in;
  &:focus {
    border-bottom: solid 1px ${colors.deepLavender};
  }
`;
const DateContainer = styled.div`
  position: absolute;
  top: -.2rem;
  left: 0;
  z-index: 9;
  background: #fff;
  padding: 1.7rem 0;
  width: 32rem;
  box-shadow: 0 2px 30px 0 rgba(0, 0, 0, 0.1);
`;
const CurrentDate = styled.div`
  font-size: 1.4rem;
  font-weight: 700;
  margin: 0 0 1.8rem 6.2rem;
`;
const DateRow = styled.div`
  display: flex;
  align-items: center;
  padding: 0 2.7rem 0 2.4rem;
  margin: 0 0 2rem;
  font-size: 1.rem;
  cursor: pointer;

  svg {
    margin-right: 1.8rem;
  }
`;
const DayName = styled.div`
  margin-left: auto;
  color: ${colors.lightGreyBlue};
`;