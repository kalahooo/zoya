import Link from "next/link";
import colors from "../../../colors";
import styled from "@emotion/styled";

const Crumbs = styled.nav`
  display: flex;
  padding: 6rem 0 4rem 3rem;
  font-weight: 700;
`;
const CrumbsLink = styled.a`
  display: inline-flex;
  align-items: center;
  text-decoration: none;
  color: ${colors.charcoalGrey};

  &::after {
    content: "/";
    margin: 0 1em;
  }
`;
Crumbs.CrumbsCurrent = styled.span`
  color: ${colors.lightGreyBlue};
`;

Crumbs.CrumbsLink = ({ href, children }) => (
  <Link href={href} passHref>
    <CrumbsLink>{children}</CrumbsLink>
  </Link>
);

export default Crumbs;
