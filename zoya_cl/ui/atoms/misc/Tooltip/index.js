import React, { Children } from "react";
import styled from "@emotion/styled";
import colors from "../../../colors";

export default function Tooltip({ position, children, tooltipText }) {
  return (
    <TooltipWrap>
      {children && children}
      {tooltipText && <TooltipContent position={position}>{tooltipText}</TooltipContent>}
    </TooltipWrap>
  );
}
const TooltipContent = styled.span`
  position: absolute;
  white-space: nowrap;
  background: white;
  box-shadow: 0 0 1rem 0 #e9e9e9;
  border-radius: .4rem;
  font-size: 1rem;
  color: ${colors.lightGreyBlue};
  padding: .5rem 1.4rem;
  display: none;

  &::after {
    content: "";
    position: absolute;
    border: solid .5rem transparent;
  }

  ${({ position }) => {
    let result = `
      bottom: 100%;
      left: 50%;
      transform: translate(-50%, -1rem);

      &::after {
        border-top-color: white;
        top: 100%;
        left: 50%;
        margin-left: -.5rem;
      }
    `;
    
    if (position === "left") {
      result = `
        right: 100%;
        top: 50%;
        transform: translate(-1rem, -50%);

        &::after {
          border-left-color: white;
          left: 100%;
          top: 50%;
          margin-top: -.5rem;
        }
      `;
    } else if (position === "right") {
      result = `
        left: 100%;
        top: 50%;
        transform: translate(1rem, -50%);

        &::after {
          border-right-color: white;
          right: 100%;
          top: 50%;
          margin-top: -.5rem;
        }
      `;
    } else if (position === "bottom") {
      result = `
        top: 100%;
        left: 50%;
        transform: translate(-50%, 1rem);

        &::after {
          border-bottom-color: white;
          bottom: 100%;
          left: 50%;
          margin-left: -.5rem;
        }
      `;
    }
  
    return result;
  }}
`;

const TooltipWrap = styled.span`
  position: relative;
  display: inline-block;
  vertical-align: middle;

  &:hover ${TooltipContent} {
    display: block;
  }
`;