import styled from "@emotion/styled";
import theme from "../../../theme";
import colors from "../../../colors";

const Table = styled.div``;

Table.Row = styled.div`
  display: flex;
  padding-left: 3rem;
  height: 4rem;

  &:nth-of-type(even) {
    background-color: ${colors.iceBlue};
  }
`;

Table.Column = styled.div`
  width: 25%;
  font-size: 1.6rem;
  color: ${theme.font.color};
  line-height: 4rem;
`;

Table.HeadColumn = styled(Table.Column)`
  font-size: 1rem;
  line-height: 1.4;
  color: ${colors.lightGreyBlue};
  padding-top: 1.3rem;

`;

export default Table;
