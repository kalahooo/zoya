import React from "react";
import styled from "@emotion/styled";
import theme from "../../../theme";

const TextRadio = ({ name, value, label }) => (
  <>
    <Radio id={`${name}-${value}`} type="radio" name={name} value={value} hidden />
    <Label htmlFor={`${name}-${value}`}>{label}</Label>
  </>
);

const Radio = styled.input``;

const Label = styled.label`
  font-size: 1.4rem;
  line-height: 1.4;
  color: ${theme.label.color};
  margin-right: 3rem;
  cursor: pointer;
  user-select: none;

  ${Radio}:checked + & {
    font-weight: 700;
    color: ${theme.label.activeColor};
  }
`;

export default TextRadio;
