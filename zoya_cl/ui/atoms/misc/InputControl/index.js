import { css } from "@emotion/core";
import React from "react";
import colors from "../../../colors";
import styled from "@emotion/styled";

export default function InputControl({
  input,
  meta,
  placeholder = "",
  size = "sm",
  label,
  type = "text",
  disabled,
  notShrink,
  inputComponent,
  ...restProps
}) {
  const InputComponent = inputComponent || null;
  const hasError = meta.touched && meta.error;

  return (
    <Control disabled={disabled}>
      {placeholder && !(input.value && notShrink) && (
        <Label
          hasError={hasError}
          htmlFor={input.name}
          isSmall={input.value}
          size={size}
        >
          {hasError && !input.value ? meta.error : placeholder}
        </Label>
      )}
      <Input hasError={hasError} size={size}>
        {InputComponent ? (
          <InputComponent {...input} type={type} disabled={disabled} {...restProps} />
        ) : (
          <input {...input} type={type} disabled={disabled} />
        )}
      </Input>
    </Control>
  );
}

const Control = styled.div`
  position: relative;
  ${({ disabled }) =>
    disabled &&
    css`
      opacity: 0.4;
    `}
`;
const Label = styled.label`
  font-size: ${p => (p.isSmall ? "1rem" : "1.6rem")};
  color: ${colors.lightGreyBlue};
  transition: all 0.25s;
  transform: ${p =>
    p.isSmall
      ? `translateY(${p.size === "sm" ? "-2rem" : "-3rem"})`
      : "translateY(0)"};
  position: absolute;
  height: 2rem;
  line-height: 2rem;
  top: 0;
  left: 0;
  bottom: 0;
  margin: auto 0;
  white-space: nowrap;
  ${({ hasError }) =>
    hasError &&
    css`
      color: ${colors.orangeyRed};
    `}
`;
const Input = styled.div`
  input {
    width: 100%;
    display: block;
    border: 0;
    font-size: ${p => (p.size === "sm" ? "1.6rem" : "2.4rem")};
    font-weight: ${p => (p.size === "sm" ? 400 : 700)};
    line-height: 3.4rem;
    font-family: inherit;
    color: ${colors.darkGrey};
    background: transparent;
    position: relative;
    outline: none;
    border-bottom: solid 1px ${colors.lightBlueGrey};
    transition: border-bottom 0.1s ease-in;
    &:focus {
      border-bottom: solid 1px ${colors.deepLavender};
    }

    ${({ hasError }) =>
      hasError &&
      css`
        border-bottom: solid 1px ${colors.orangeyRed};
      `}
  }
`;
