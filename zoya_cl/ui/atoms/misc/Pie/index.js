import React from 'react'
import styled from "@emotion/styled";
import colors from '../../../colors';

export default function ({
  value = 33
}) {
  const perToDeg = value * 3.6;
  const valueRight = Math.min(180, Math.max(0, perToDeg));
  const valueLeft = Math.min(360, Math.max(180, perToDeg));

  return (
    <Pie>
      <SegmentRight>
        <SegmentValue style={{ transform: `rotate(${valueRight}deg)` }} />
      </SegmentRight>
      {perToDeg > 180 && (
        <SegmentLeft>
          <SegmentValue style={{ transform: `rotate(${valueLeft - 180}deg)` }} />
        </SegmentLeft>
      )}
    </Pie>
  )
}

const Pie = styled.div`
  width: 1.6rem;
  height: 1.6rem;
  display: inline-block;
  border-radius: 50%;
  background: ${colors.veryLightBlue};
  vertical-align: middle;
  position: relative;
  overflow: hidden;
`;
const SegmentRight = styled.div`
  width: 200%;
  height: 200%;
  position: absolute;
  top: -50%;
  left: 49.9%;
  overflow: hidden;
`;
const SegmentLeft = styled(SegmentRight)`
  right: 50%;
  left: auto;
  transform: rotate(180deg);
`;
const SegmentValue = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: -100%;
  transform-origin: 100% 50%;
  background: ${colors.deepLavender};
`;