import CloseIcon from "../../icons/Close";
import React from "react";
import styled from "@emotion/styled";
import theme from "../../../theme";

const Modal = ({ children, onCancel, notOverflow }) => (
  <>
    <Backdrop onClick={onCancel} />
    <Container notOverflow={notOverflow} onClick={ev => ev.stopPropagation()}>
      {onCancel && (
        <CloseButton onClick={onCancel}>
          <CloseIcon />
        </CloseButton>
      )}
      <Content>{children}</Content>
    </Container>
  </>
);

Modal.Title = styled.h4`
  font-size: 1.6rem;
  color: ${theme.font.color};
  line-height: 1.4;
  margin-bottom: 1.6rem;
`;

Modal.SubTitle = styled.h5`
  font-size: 2.4rem;
  color: ${theme.font.titleColor};
  line-height: 1.4;
  margin-bottom: 4rem;
`;

const Backdrop = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background: ${theme.modal.bg};
  opacity: 0.3;
  z-index: 10;
  margin: 0 !important;
`;

const Container = styled.div`
  background: #fff;
  max-height: 100%;
  overflow: ${p => p.notOverflow ? "visible" : "auto"};
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  box-shadow: ${theme.modal.boxShadow};
  z-index: 11;
`;

const CloseButton = styled.div`
  cursor: pointer;
  position: absolute;
  right: 27px;
  top: 27px;
  width: 16px;
  height: 16px;
`;

const Content = styled.div`
  padding: 7rem 10rem 10rem;
`;

export default Modal;
