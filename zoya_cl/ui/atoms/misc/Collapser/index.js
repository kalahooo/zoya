import { useState } from "react";

const Collapser = ({ children, opened = false }) => {
  const [isOpened, toggle] = useState(opened);
  return children({
    isOpened,
    toggle
  });
};

export default Collapser;
