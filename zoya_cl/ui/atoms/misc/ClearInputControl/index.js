import React from "react";
import colors from "../../../colors";
import styled from "@emotion/styled";
import Close from "../../icons/Close";
import { isNumeric } from "../../../../utils/validation-rules";

export default function ClearInputControl({
  input,
  placeholder = "",
  label,
  type = "text"
}) {
  const handleClear = () => input.onChange('');
  const handleChange = e => {
    const value = e.target.value;

    if (type === "number") {
      if (value.length && !isNumeric(value)) {
        return;
      }
    }

    input.onChange(value);
  }

  return (
    <Control>
      <Input {...input} placeholder={placeholder} hasValue={input.value} onChange={handleChange} />
      {input.value && <ClearField onClick={handleClear}><Close /></ClearField>}
    </Control>
  );
}

const Control = styled.div`
  position: relative;
`;
const Input = styled.input`
  width: 100%;
  display: block;
  border: 0;
  font-size: ${p => (p.hasValue ? "2.4rem" : "1.6rem")};
  font-weight: ${p => (p.hasValue ? 700 : 400)};
  line-height: ${p => (p.hasValue ? "4.6rem" : "3.6rem")};
  font-family: inherit;
  color: ${colors.darkGrey};
  background: transparent;
  position: relative;
  outline: none;
  border-bottom: solid 1px ${colors.lightBlueGrey};
  transition: border-bottom 0.1s ease-in;

  &:focus {
    border-bottom: solid 1px ${colors.deepLavender};
  }
`;
const ClearField = styled.span`
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  cursor: pointer;
  display: flex;
  align-items: center;
  padding: 0 .5rem;
`;
