import React from "react";
import colors from "../../../colors";
import styled from "@emotion/styled";
import Checked from "../../icons/Checked";
import Unchecked from "../../icons/Unchecked";

const Checkbox = ({ children, input }) => {
  return (
    <Label>
      <input type="checkbox" {...input} />
      <span>
        <IconChecked><Checked /></IconChecked>
        <IconUchecked><Unchecked /></IconUchecked>
        {children}
      </span>
    </Label>
  );
};

export default Checkbox;

const IconUchecked = styled.span`
  position: absolute;
  left: 0;
  top: 0;
`;
const IconChecked = styled(IconUchecked)`
  position: absolute;
  left: 0;
  top: 0;
  z-index: 2;
  opacity: 0;
  transition: opacity .1s;
`;
const Label = styled.label`
  position: relative;
  user-select: none;
  overflow: hidden;
  padding-left: 4rem;
  line-height: ${20 / 14};
  cursor: pointer;

  input {
    position: absolute;
    left: -999px;

    &:checked + span {
      color: ${colors.deepLavender};

      ${IconChecked} {
        opacity: 1;
      }
    }
  }
  span {
    font-size: 1.4rem;
    color: ${colors.lightGreyBlue};
    transition: color .1s;
  }
`;
