import styled from "@emotion/styled";
import theme from "../theme";

const Label = styled.label`
  display: block;
  font-size: 1rem;
  color: ${theme.label.color};
  margin-bottom: 1rem;
`;

export default Label
