import styled from "@emotion/styled";
import theme from "../../theme";

export const ButtonPrimary = styled.button`
  border: 0;
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: ${theme.buttons.default.bg};
  color: ${theme.buttons.default.color};
  text-transform: uppercase;
  letter-spacing: 0.1rem;
  border-radius: 0.5rem;
  height: 5rem;
  box-shadow: ${theme.buttons.default.boxShadow};
  padding: 0 5rem;
  font-weight: 700;
  font-size: 1rem;
  transition: all 0.25s;
  cursor: pointer;
  text-decoration: none;

  &:hover {
    box-shadow: ${theme.buttons.default.boxShadowHover};
  }

  &:active {
    box-shadow: ${theme.buttons.default.boxShadowActive};
  }

  &[disabled] {
    opacity: 0.3;
    cursor: default;
    box-shadow: none;
  }
`;

export default ButtonPrimary;
