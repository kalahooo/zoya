import styled from "@emotion/styled";
import theme from "../../theme";

export const Button = styled.button`
  border: 0;
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  color: ${theme.buttons.transparent.color};
  border: solid 1px ${theme.buttons.transparent.borderColor};
  text-transform: uppercase;
  letter-spacing: 0.1rem;
  border-radius: 0.5rem;
  height: 5rem;
  padding: 0 5rem;
  font-weight: 700;
  font-size: 1rem;
  transition: all 0.25s;

  &:hover {
    background: ${theme.buttons.transparent.bgHover};
    color: ${theme.buttons.transparent.colorHover};
    border: solid 1px ${theme.buttons.transparent.borderColorHover};
  }

  &:active {
    background: ${theme.buttons.transparent.bgActive};
    color: ${theme.buttons.transparent.colorActive};
    border: solid 1px ${theme.buttons.transparent.borderColorActive};
  }

  &[disabled] {
    background: ${theme.buttons.transparent.bg};
    color: ${theme.buttons.transparent.color};
    border: solid 1px ${theme.buttons.transparent.borderColor};
    opacity: 0.5;
    cursor: default;
  }
`;

export default Button;
