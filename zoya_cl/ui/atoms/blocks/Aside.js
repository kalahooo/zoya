import styled from "@emotion/styled";

const Aside = styled.aside`
  width: 41.9rem;
  border-left: solid 1px #f0f3f6;
`;

Aside.Group = styled.div`
  padding: 4rem 4rem 4rem 4rem;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);

  &:last-of-type {
    border-bottom: none;
  }

  button,
  a {
    width: 23rem;
  }
`;
export default Aside;
