import styled from "@emotion/styled";

export const BlockPreview = styled.div`
  background: #fff;
  width: 72rem;
  margin: 0 0 3rem;
  padding: 2.3rem 3rem 1rem;
`;

export default BlockPreview;
