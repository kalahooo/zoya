import styled from "@emotion/styled";

const Head = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 0 3rem 3rem;
  align-items: center;

  h1,
  h2,
  h3,
  h4 {
    margin-bottom: 0;
  }
`;

export default Head;
