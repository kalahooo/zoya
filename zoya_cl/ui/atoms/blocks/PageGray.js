import styled from "@emotion/styled";

export const PageGray = styled.div`
  padding: 3rem;
  background: #f0f3f6;
`;

export default PageGray;
