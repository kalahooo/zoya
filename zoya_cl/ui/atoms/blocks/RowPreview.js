import styled from "@emotion/styled";

export const RowPreview = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export default RowPreview;