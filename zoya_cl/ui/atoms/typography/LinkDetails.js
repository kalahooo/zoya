import ArrowRight from "../icons/ArrowRight";
import React from "react";
import colors from "../../colors";
import styled from "@emotion/styled";

export const LinkDetails = ({ children }) => (
  <Details>
    {children}
    <ArrowRight />
  </Details>
);

const Details = styled.span`
  display: inline-flex;
  font-size: 1.4rem;
  align-items: center;
  color: ${colors.deepLavender};
  font-weight: 700;
  transition: color 0.25s;
  cursor: pointer;

  a {
    text-decoration: none;
  }

  svg {
    margin-left: 1.2rem;
    transition: all 0.25s;
  }

  &:hover {
    color: ${colors.purply};

    svg {
      transform: translate3d(0.4rem, 0, 0);
    }
  }
`;

export default LinkDetails;
