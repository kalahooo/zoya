import styled from "@emotion/styled";
import colors from "../../colors";

export const H1 = styled.h1`
  font-size: 5rem;
  font-weight: 700;
  line-height: ${68 / 50};
  color: ${colors.darkGrey};
  margin: 0 0 2rem;
`;

export const H2 = styled.h2`
  font-size: 2.4rem;
  font-weight: 700;
  line-height: ${33 / 24};
  color: ${colors.darkGrey};
  margin: 0 0 1.6rem;
`;

export const H3 = styled.h3`
  font-size: 1.8rem;
  font-weight: 400;
  line-height: ${24 / 18};
  color: ${colors.darkGrey};
  margin: 0 0 1.2rem;
`;

export const H4 = styled.h4`
  font-size: 1.6rem;
  font-weight: 700;
  line-height: ${22 / 16};
  color: ${colors.darkGrey};
  margin: 0 0 1rem;
`;

export default {
  H1,
  H2,
  H3,
  H4
};
