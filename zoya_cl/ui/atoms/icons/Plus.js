import React from "react";

export default function Plus() {
  return (
    <svg
      style={{ width: "2rem", height: "2rem" }}
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-5-5h30v30H-5z" />
        <g fill="#000" fillRule="nonzero">
          <path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0zm0 18.782c-4.842 0-8.782-3.94-8.782-8.782 0-4.842 3.94-8.782 8.782-8.782 4.842 0 8.782 3.94 8.782 8.782 0 4.842-3.94 8.782-8.782 8.782z" />
          <path
            d="M14.808 9.615h-4.423v-4.23a.385.385 0 1 0-.77 0v4.23H5.192a.385.385 0 1 0 0 .77h4.423V15a.385.385 0 1 0 .77 0v-4.615h4.423a.385.385 0 1 0 0-.77z"
            stroke="#000"
            strokeWidth=".5"
          />
        </g>
      </g>
    </svg>
  );
}
