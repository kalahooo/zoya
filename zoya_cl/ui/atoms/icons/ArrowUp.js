import React from "react";

export default function ArrowUp() {
  return (
    <svg
      style={{ width: "1.8rem", height: "1rem" }}
      viewBox="0 0 18 10"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-6-9h30v30H-6z" />
        <path
          d="M9 3.343L2.636 9.707a1 1 0 0 1-1.414-1.414l7.07-7.071a.997.997 0 0 1 1.415 0l7.071 7.07a1 1 0 0 1-1.414 1.414v.001L9 3.343z"
          fill="#C6C9CF"
        />
      </g>
    </svg>
  );
}
