import React from "react";

export default function Delete() {
  return (
    <svg
      style={{ width: "2.2rem", height: "2.2rem" }}
      viewBox="0 0 22 22"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-4-4h30v30H-4z" />
        <g fillRule="nonzero" stroke="#C6C9CF">
          <path
            d="M3.929 3.929c-3.899 3.899-3.9 10.243 0 14.142 3.9 3.9 10.243 3.899 14.142 0 3.899-3.899 3.9-10.243 0-14.142-3.9-3.9-10.243-3.899-14.142 0z"
            strokeWidth="1.2"
          />
          <path
            d="M13.393 8.108l-2.502 2.502-2.393-2.393a.308.308 0 1 0-.435.435l2.393 2.393-2.502 2.502a.308.308 0 1 0 .435.435l2.502-2.502 2.611 2.611a.308.308 0 1 0 .435-.435l-2.61-2.61 2.501-2.503a.308.308 0 1 0-.435-.435z"
            fill="#C6C9CF"
            strokeWidth=".5"
          />
        </g>
      </g>
    </svg>
  );
}
