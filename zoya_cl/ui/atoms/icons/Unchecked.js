import React from "react";

export default function Unchecked() {
  return (
    <svg
      style={{ width: "2rem", height: "2rem" }}
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="#C6C9CF" fillRule="nonzero">
        <path d="M1.2 4.995v10.01A3.795 3.795 0 0 0 4.995 18.8h10.01a3.795 3.795 0 0 0 3.795-3.795V4.995A3.795 3.795 0 0 0 15.005 1.2H4.995A3.795 3.795 0 0 0 1.2 4.995zm-1.2 0A4.995 4.995 0 0 1 4.995 0h10.01A4.995 4.995 0 0 1 20 4.995v10.01A4.995 4.995 0 0 1 15.005 20H4.995A4.995 4.995 0 0 1 0 15.005V4.995z" />
        <path d="M14.88 7.085l-6.151 6.15a.138.138 0 0 1-.196 0L5.04 9.744a.139.139 0 0 1 0-.196l.848-.848a.139.139 0 0 1 .196 0l2.546 2.546 5.204-5.204a.139.139 0 0 1 .196 0l.849.848a.138.138 0 0 1 0 .196z" />
      </g>
    </svg>
  );
}
