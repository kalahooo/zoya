import React from "react";

export default function Search() {
  return (
    <svg
      style={{ width: "2.1rem", height: "2.2rem" }}
      viewBox="0 0 21 22"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M15.14 14.727l5.567 5.566a1 1 0 0 1-1.414 1.414l-5.681-5.68a8.75 8.75 0 1 1 1.529-1.3zM8.75 2A6.758 6.758 0 0 0 2 8.75a6.758 6.758 0 0 0 6.75 6.75 6.758 6.758 0 0 0 6.75-6.75A6.758 6.758 0 0 0 8.75 2z"
        fill="#212224"
        fillRule="evenodd"
      />
    </svg>
  );
}
