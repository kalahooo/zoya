import React from "react";

export default function Undone() {
  return (
    <svg
      style={{ width: "2rem", height: "2rem" }}
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <rect
          height="18.8"
          rx="9.4"
          stroke="#C6C9CF"
          strokeWidth="1.2"
          width="18.8"
          x=".6"
          y=".6"
        />
        <path
          d="M14.88 7.085l-6.151 6.15a.138.138 0 0 1-.196 0L5.04 9.744a.139.139 0 0 1 0-.196l.848-.848a.139.139 0 0 1 .196 0l2.546 2.546 5.204-5.204a.139.139 0 0 1 .196 0l.849.848a.138.138 0 0 1 0 .196z"
          fill="#C6C9CF"
          fillRule="nonzero"
        />
      </g>
    </svg>
  );
}
