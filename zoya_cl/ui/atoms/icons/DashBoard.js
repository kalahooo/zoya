import React from "react";

export default function DashBoard() {
  return (
    <svg
      style={{ width: "2.2rem", height: "1.9rem" }}
      viewBox="0 0 22 19"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd" stroke="#FFF" strokeWidth="1.6">
        <rect height="9.4" rx="1" width="12.4" x=".8" y=".8" />
        <rect height="4.4" rx="1" width="4.4" x=".8" y="13.8" />
        <rect height="9.4" rx="1" width="4.4" x="16.8" y=".8" />
        <rect height="4.4" rx="1" width="12.4" x="8.8" y="13.8" />
      </g>
    </svg>
  );
}
