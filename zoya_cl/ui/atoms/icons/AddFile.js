import React from "react";

export default function AddFile() {
  return (
    <svg
      height="20"
      viewBox="0 0 16 20"
      width="16"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-7-5h30v30H-7z" />
        <path
          d="M15.38 5.717l.124.141-.124-.33v.189zM10.79.507h.227l-.376-.17.15.17zm0 0l4.59 5.21v11.652c0 1.172-.937 2.124-2.083 2.124H2.72c-1.146 0-2.083-.952-2.083-2.124V2.631c0-1.172.937-2.124 2.083-2.124h8.07zm.067.438l4.138 4.696h-4.138V.945z"
          fillRule="nonzero"
          stroke="#C6C9CF"
        />
        <path
          d="M8.573 8.186H7.342v2.648H4.735v1.25h2.607v2.647h1.23v-2.647h2.607v-1.25H8.573z"
          fill="#C6C9CF"
        />
      </g>
    </svg>
  );
}
