import React from "react";

export default function Closed() {
  return (
    <svg
      style={{ width: "1.6rem", height: "1.6rem" }}
      viewBox="0 0 16 16"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-7-7h30v30H-7z" />
        <path
          d="M8 7.587L1.498 1.085a.292.292 0 1 0-.412.413L7.587 8l-6.502 6.502a.292.292 0 0 0 .413.412L8 8.413l6.502 6.502a.292.292 0 0 0 .412-.413L8.413 8l6.502-6.502a.292.292 0 0 0-.413-.412L8 7.587z"
          fill="#C6C9CF"
          fillRule="nonzero"
          stroke="#C6C9CF"
          strokeWidth="1.5"
        />
      </g>
    </svg>
  );
}
