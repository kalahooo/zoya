import React from "react";

export default function ArrowUp() {
  return (
    <svg
      height="10"
      viewBox="0 0 18 10"
      width="18"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-6 21h30V-9H-6z" />
        <path
          d="M9 3.343l6.364 6.364a1 1 0 0 0 1.414-1.414l-7.07-7.071a.997.997 0 0 0-1.415 0l-7.071 7.07a1 1 0 0 0 1.414 1.414v.001L9 3.343z"
          fill="#C6C9CF"
        />
      </g>
    </svg>
  );
}
