import React from "react";

export default function ArrowRight() {
  return (
    <svg
      style={{ width: "1rem", height: "1.8rem" }}
      viewBox="0 0 10 18"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd">
        <path d="M-11 24V-6h30v30z" />
        <path
          d="M6.657 9L.293 2.636a1 1 0 0 1 1.414-1.414l7.071 7.07a.997.997 0 0 1 0 1.415l-7.07 7.071a1 1 0 0 1-1.414-1.414H.292L6.657 9z"
          fill="currentColor"
        />
      </g>
    </svg>
  );
}
