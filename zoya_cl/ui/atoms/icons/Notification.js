import React from "react";

export default function Notification() {
  return (
    <svg
      height="23"
      style={{ width: "2.2rem", height: "2.3rem" }}
      viewBox="0 0 22 23"
      width="22"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd" transform="translate(-5 -4)">
        <path d="M0 0h30v30H0z" />
        <path
          d="M15.5 27c1.381 0 2.5-.895 2.5-2h-5c0 1.105 1.119 2 2.5 2zM19.26 7.66a7.15 7.15 0 0 0-2.385-1.018v-.824C16.875 4.812 16.038 4 15 4c-1.037 0-1.875.812-1.875 1.818v.824C9.544 7.467 7.5 10.576 7.5 14.303v6.06L5 22.789V24h20v-1.212l-2.5-2.424v-1.385a6 6 0 0 1-3.24-11.32z"
          fill="#C6C9CF"
          fillRule="nonzero"
        />
        <circle cx="22" cy="13" fill="#F44236" r="5" />
      </g>
    </svg>
  );
}
