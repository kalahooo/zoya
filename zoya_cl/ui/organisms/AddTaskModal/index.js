// @flow
import { ButtonTransparent as B, ButtonPrimary } from "../../atoms/buttons";
import { Field, Form } from "react-final-form";
import { format } from "date-fns";
import { graphql } from "react-apollo";
import { required } from "../../../utils/validation-rules";
import { scheduledContact } from "./scheduledContact.graphql";
import Cleave from "cleave.js/react";
import DatePicker from "../../atoms/misc/DatePicker";
import InputControl from "../../atoms/misc/InputControl";
import Modal from "../../atoms/misc/Modal";
import React from "react";
import Switch from "../../atoms/misc/Switch";
import styled from "@emotion/styled";
import type { ScheduledContactInput } from "../../../__generated__/scheduledContact";

const AddTaskModal = ({
  onCancel,
  onSuccess,
  scheduledContact,
  leadId,
  refetchQueries
}: {
  leadId: string,
  onCancel?: Function,
  onSuccess?: Function,
  scheduledContact: Function
}) => {
  function handleSubmit(values) {
    let date = new Date(values.date);
    date.setHours(values.time.split(":")[0]);
    date.setMinutes(values.time.split(":")[1]);
    let scheduledDate = date.toISOString();

    const input: ScheduledContactInput = {
      leadId,
      subject: values.subject,
      contactType: values.contactType,
      scheduledDate
    };
    return scheduledContact({ variables: { input }, refetchQueries }).then(
      onSuccess
    );
  }

  return (
    <Modal notOverflow onCancel={onCancel}>
      <Modal.Title>Вы успешно добавили нового лида.</Modal.Title>
      <Modal.SubTitle>Хотите запланировать с ним контакт?</Modal.SubTitle>

      <Form
        onSubmit={handleSubmit}
        render={({ handleSubmit, hasValidationErrors, submitting }) => (
          <TaskForm onSubmit={handleSubmit}>
            <Field
              component={Switch}
              defaultValue={"PHONECALL"}
              items={[
                { label: "Телефонный звонок", value: "PHONECALL" },
                { label: "Отправить e-mail", value: "EMAIL" },
                { label: "Назначить встречу", value: "MEETING" }
              ]}
              name="contactType"
              validate={required()}
            />

            <DateTimeSelect>
              <Field
                component={InputControl}
                inputComponent={Cleave}
                name="time"
                options={{ time: true, timePattern: ["h", "m"] }}
                placeholder="Время"
                validate={required()}
              />
              <Field
                component={DatePicker}
                name="date"
                placeholder="Дата"
                validate={required()}
              />
            </DateTimeSelect>

            <Field
              component={InputControl}
              name="subject"
              placeholder="Тема"
              validate={required()}
            />

            <Actions>
              <ButtonPrimary
                disabled={hasValidationErrors || submitting}
                onClick={handleSubmit}
              >
                Добавить Задачу
              </ButtonPrimary>
              <ButtonTransparent onClick={onCancel} type="button">
                Нет, спасибо
              </ButtonTransparent>
            </Actions>
          </TaskForm>
        )}
      />
    </Modal>
  );
};

export default graphql(scheduledContact, { name: "scheduledContact" })(
  AddTaskModal
);

const Actions = styled.div`
  display: flex;
  padding-top: 3rem;
`;
const DateTimeSelect = styled.div`
  display: flex;

  align-items: center;

  & > * {
    margin-right: 2rem;
    width: 12rem;
  }
`;
const ButtonTransparent = styled(B)`
  border: none;
  &:hover {
    border: none;
  }
`;
const TaskForm = styled.form`
  & > * {
    margin-bottom: 3rem;
    &:last-child {
      margin-bottom: 0;
    }
  }
`;
