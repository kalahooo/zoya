import { useState } from "react";

const Tabs = ({ children, defaultIndex = 0 }) => {
  const [selectedIndex, setSelectedIndex] = useState(defaultIndex);
  return children({
    selectedIndex,
    setSelectedIndex
  });
};

export default Tabs;
