import colors from "./colors";

const theme = {
  font: {
    family: "'Open Sans', sans-serif",
    sizeBase: "1.4rem",
    color: colors.darkGrey,
    lineHeight: 1.5
  },
  label: {
    color: colors.lightGreyBlue,
    activeColor: colors.deepLavender
  },
  input: {
    border: colors.deepLavender,
    radio: {
      default: colors.lightGreyBlue,
      actvie: colors.deepLavender
    }
  },
  modal: {
    bg: colors.darkGrey,
    boxShadow: "0 .2rem 3rem 0 rgba(0, 0, 0, 0.1)",
  },
  buttons: {
    default: {
      bg: colors.deepLavender,
      color: colors.white,
      boxShadow: "0 1rem 2rem 0 rgba(0, 0, 0, 0.2)",
      boxShadowHover: "0 2rem 3rem 0 rgba(0, 0, 0, 0.3)",
      boxShadowActive: "0 .5rem .5rem 0 rgba(0, 0, 0, 0.2)"
    },
    transparent: {
      borderColor: colors.lightBlueGrey,
      bg: colors.white,
      color: colors.lightBlueGrey,
      borderColorHoder: colors.lightGreyBlue,
      bgHover: colors.white,
      colorHover: colors.charcoalGrey,
      borderColorActive: colors.lightBlueGrey,
      bgActive: colors.white,
      colorActive: colors.lightGreyBlue
    }
  }
};

export default theme;
