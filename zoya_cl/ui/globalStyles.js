import colors from "./colors";
import css from "@emotion/css";
import theme from "./theme";

export default css`
  @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=cyrillic");

  html {
    height: 100%;
    min-height: 100vh;
    font-size: 10px;
    font-size: ${10 / 19.2}vw;
    padding: 0;
    margin: 0;
  }
  body {
    min-height: 100vh;
    color: ${theme.font.color};
    font-size: ${theme.font.sizeBase};
    font-family: ${theme.font.family};
    line-height: ${theme.font.lineHeight};
    background: ${colors.white};
    padding: 0;
    margin: 0;
  }
  * {
    box-sizing: border-box;
  }
  input,
  a,
  select,
  button {
    outline: none;
    font-size: 1em;
  }
  button,
  a {
    cursor: pointer;
  }
`;
