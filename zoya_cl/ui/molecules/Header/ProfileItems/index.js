import Notification from "../../../atoms/icons/Notification";
import React from "react";
import styled from "@emotion/styled";
import useAuth from "../../../../core/Auth/useAuth";

const ProfileItems = () => {
  const [auth] = useAuth();

  return (
    <ProfileItemsWrap>
      <NotificationWrap>
        <Notification />
      </NotificationWrap>
      <Name>{auth?.firstName}</Name>
      <Avatar style={{ backgroundImage: `url("http://i.pravatar.cc/300")` }} />
    </ProfileItemsWrap>
  );
};

export default ProfileItems;

const Name = styled.div`
  font-weight: bold;
`;
const ProfileItemsWrap = styled.div`
  display: flex;
  margin-left: auto;
  align-items: center;
  padding-right: 15rem;
  justify-content: flex-end;
  > * {
    margin-left: 3rem;
  }
`;
const NotificationWrap = styled.div`
  height: 3rem;
  width: 3rem;
  > * {
    max-width: 100%;
    max-height: 100%;
  }
`;
const Avatar = styled.div`
  height: 4.6rem;
  width: 4.6rem;
  border-radius: 50%;
  background-position: 50%;
  background-size: cover;
`;
