import ProfileItems from "./ProfileItems";
import React from "react";
import Search from "../../atoms/icons/Search";
import styled from "@emotion/styled";

const Header = () => {
  return (
    <HeaderWrap>
      <IconWrap>
        <Search />
      </IconWrap>
      <input
        placeholder="Укажите ИНН или ОГРН (ОГРНИП) или наименование ЮЛ, ФИО ИП"
        type="text"
      />

      <ProfileItems />
    </HeaderWrap>
  );
};

export default Header;

const IconWrap = styled.div`
  width: 6rem;
  display: flex;
  justify-content: center;
  align-items: center;
  > * {
    width: 2.3rem;
    height: 2.3rem;
`;
const HeaderWrap = styled.header`
  top: 0;
  right: 0;
  left: 0;
  background: #fff;
  z-index: 9;
  position: absolute;
  min-height: 7rem;
  box-shadow: 0 2px 1px 0 #ebeef1;
  display: flex;
  align-items: center;

  input {
    min-width: 100rem;
    border: none;
    outline: none;
    &::placeholder {
      color: #aaaeb3;
    }
  }
`;
