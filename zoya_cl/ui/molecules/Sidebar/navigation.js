const navigation = [
  {
    title: "Рабочий стол",
    id: 1,
    iconName: "dashboard"
  },
  {
    title: "Лиды",
    id: 2,
    iconName: "dashboard",
    href: "/account/leads"
  },
  {
    title: "Договоры",
    id: 3,
    iconName: "dashboard"
  },
  {
    title: "Клиенты",
    id: 4,
    iconName: "dashboard"
  },
  {
    title: "Банки",
    id: 5,
    iconName: "dashboard"
  },
  {
    title: "Отчеты",
    id: 6,
    iconName: "dashboard"
  }
];

export default navigation;
