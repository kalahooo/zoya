import Link from "next/link";
import React from "react";
import colors from "../../colors";
import navigation from "./navigation";
import styled from "@emotion/styled";

import Logo1x from "./images/logo.png";
import Logo2x from "./images/logo_2x.png";
import Logo3x from "./images/logo_3x.png";
import LogoSmall from "./images/logo-small.png";

import DashBoard from "../../atoms/icons/DashBoard";
import Settings from "../../atoms/icons/Settings";

class Sidebar extends React.Component {
  state = {
    isSidebarOpened: false
  };

  handleMouseEnter = () => {
    this.timeout = setTimeout(() => {
      this.setState({ isSidebarOpened: true });
    }, 100);
  };

  handleMouseLeave = () => {
    clearTimeout(this.timeout);
    this.setState({ isSidebarOpened: false });
  };

  getIcon = name => {
    const icons = {
      dashboard: DashBoard
    };

    if (!icons[name]) {
      return null;
    }

    const Icon = icons[name];

    return <Icon />;
  };

  render() {
    const { isSidebarOpened } = this.state;

    return (
      <SidebarBlock
        isOpened={isSidebarOpened}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
      >
        <Logo>
          <LogoSmallImg alt="logo small" src={LogoSmall} />
          <LogoImg
            alt="logo"
            isOpened={isSidebarOpened}
            src={Logo1x}
            srcset={`${Logo2x} 2x, ${Logo3x} 3x`}
          />
        </Logo>
        <Menu>
          {navigation.map(({ id, title, iconName, href }) => (
            <MenuItem key={id}>
              <Link href={href || ""} passHref>
                <a>
                  <MenuIcon>{this.getIcon(iconName)}</MenuIcon>
                  <MenuTitle isOpened={isSidebarOpened}>{title}</MenuTitle>
                </a>
              </Link>
            </MenuItem>
          ))}
        </Menu>
        <SettingsItem>
          <MenuIcon>
            <Settings />
          </MenuIcon>
          <MenuTitle isOpened={isSidebarOpened}>Настройки</MenuTitle>
        </SettingsItem>
      </SidebarBlock>
    );
  }
}

export default Sidebar;

const Menu = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  width: 100%;
  overflow: hidden;
`;
const MenuItem = styled.li`
  height: 7rem;
  border-top: solid 1px rgba(0, 0, 0, 0.1);
  width: 100%;
  position: relative;
  cursor: pointer;
  background: ${colors.deepLavender};
  transition: background 0.25s;

  &:hover {
    background: ${colors.purply};
  }
`;
const Logo = styled.div`
  height: 7rem;
`;
const LogoSmallImg = styled.img`
  position: absolute;
  width: 2.7rem;
  height: 2.4rem;
  left: 2.3rem;
  top: 2.3rem;
`;
const LogoImg = styled.img`
  position: absolute;
  width: 19.8rem;
  height: 2.4rem;
  left: 2.3rem;
  top: 2.3rem;
  transition: opacity 0.25s;
  opacity: ${p => (p.isOpened ? 1 : 0)};
`;
const MenuIcon = styled.div`
  width: 2.3rem;
  height: 2.3rem;
  top: 0;
  bottom: 0;
  margin: auto 0;
  left: 2.4rem;
  position: absolute;
`;
const MenuTitle = styled.span`
  position: absolute;
  left: 7rem;
  height: 3rem;
  line-height: 3rem;
  top: 0;
  bottom: 0;
  margin: auto 0;
  color: #fff;
  font-size: 1.4rem;
  font-weight: 700;
  white-space: nowrap;
  transition: opacity 0.25s;
  opacity: ${p => (p.isOpened ? 1 : 0)};
`;
const SidebarBlock = styled.div`
  background: ${colors.deepLavender};
  width: ${p => (p.isOpened ? "27rem" : "7rem")};
  transition: width 0.25s;
  position: relative;
  z-index: 10;
`;
const SettingsItem = styled(MenuItem)`
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
  overflow: hidden;
`;
