import Cookies from "js-cookie";
import React, { useEffect } from "react";
import Router from "next/router";

export default function() {
  useEffect(() => {
    Cookies.remove("token");
    Router.replace("/auth/login");
  }, []);
  return null;
}
