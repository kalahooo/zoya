import { ApolloProvider } from "react-apollo";
import { AuthProvider } from "../core/Auth";
import { Global } from "@emotion/core";
import App, { Container } from "next/app";
import React from "react";
import globalStyles from "../ui/globalStyles";
import withApolloClient from "../lib/with-apollo-client";

class MyApp extends App {
  render() {
    const { Component, pageProps, apolloClient } = this.props;
    return (
      <Container>
        <ApolloProvider client={apolloClient}>
          <AuthProvider client={apolloClient}>
            <Component {...pageProps} />
          </AuthProvider>
        </ApolloProvider>
        <Global styles={globalStyles} />
      </Container>
    );
  }
}

export default withApolloClient(MyApp);
