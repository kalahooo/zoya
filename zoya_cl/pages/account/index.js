import { withAuthSync } from "../../core/Auth/withAuth";
import Account from "../../features/account/pages";

export default withAuthSync(Account);
