import { withAuthSync } from "../../../core/Auth/withAuth";
import LeadsList from "../../../features/leads/list/pages";
const LeadsListWithAuth = withAuthSync(LeadsList);

LeadsListWithAuth.getInitialProps = function(ctx) {
  return { action: ctx.query.action };
};

export default LeadsListWithAuth;
