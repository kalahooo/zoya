import { tmpUser } from "../../features/auth/login/ogranisms";
import { useContext, useEffect } from "react";
import AuthContext from ".";

let initialized = false;

function useAuth() {
  const [auth, setAuth, client] = useContext(AuthContext); // eslint-disable-line

  useEffect(() => {
    if (!auth && !initialized) {
      initialized = true;

      // client
      //   .query({
      //     query: UserFetch,
      //     fetchPolicy: "network-only"
      //   })
      //   .then(result => {
      //     setAuth(result.data.account.info || {});
      //   });
      setTimeout(() => {
        setAuth(tmpUser);
      }, 100);
    }
  }, [auth]);

  return [auth, setAuth];
}

export default useAuth;
