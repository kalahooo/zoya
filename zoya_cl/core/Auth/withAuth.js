import Cookies from "js-cookie";
import React, { useEffect } from "react";
import Router from "next/router";

export function withAuthSync(Component) {
  return props => {
    useEffect(() => {
      if (!Cookies.get("token")) {
        Router.push("/auth/login");
      }
    }, []);

    return <Component {...props} />;
  };
}
