// @flow
import React, { useState } from "react";
import type { Node } from "react";

// $FlowIgnore
const AuthContext = React.createContext();

type Props = {
  children: Node
};
export const AuthProvider = ({ children, client }: Props) => {
  const [auth, setAuth] = useState();

  return (
    <AuthContext.Provider value={[auth, setAuth, client]}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
